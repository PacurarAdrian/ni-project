/*
 * Stear.c
 *
 *  Created on: Sep 1, 2015
 *      Author: So
 */


#include "StearLeft.h"
#include <stdio.h>
#include "MyRio.h"
#include "PWM.h"
#include <time.h>



extern NiFpga_Session myrio_session;

/**
 * Overview:
 * Demonstrates stearing the robot to the left.
 *
 * Instructions:
 * 1. Connect the signal wire of the left motor to Connector A, PWM0.
 * 2. Connect the signal wire of the right motor to Connector A, PWM1.
 * 2. Run this program.
 *
 * Output:
 * The program generates two PWM signals that will stear the robot to left.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */

void StearLeft()
{

    NiFpga_Status status;

	MyRio_Pwm pwmA0;
	MyRio_Pwm pwmA1;

    uint8_t selectReg1;
    uint8_t selectReg2;



    printf("Stear left\n");

    /*
     * Initialize the PWM struct with registers from the FPGA personality.
     */
    pwmA0.cnfg = PWMA_0CNFG;
    pwmA0.cs = PWMA_0CS;
    pwmA0.max = PWMA_0MAX;
    pwmA0.cmp = PWMA_0CMP;
    pwmA0.cntr = PWMA_0CNTR;


    /*
    * Initialize the PWM struct with registers from the FPGA personality.
    */
   pwmA1.cnfg = PWMA_1CNFG;
   pwmA1.cs = PWMA_1CS;
   pwmA1.max = PWMA_1MAX;
   pwmA1.cmp = PWMA_1CMP;
   pwmA1.cntr = PWMA_1CNTR;


    /*
     * Set the waveform, enabling the PWM onboard device.
     */
    Pwm_Configure(&pwmA0, Pwm_Invert | Pwm_Mode,
            Pwm_NotInverted | Pwm_Enabled);

    /*
     * Set the waveform, enabling the PWM onboard device.
     */
    Pwm_Configure(&pwmA1, Pwm_Invert | Pwm_Mode,
            Pwm_NotInverted | Pwm_Enabled);

    /*
     * Set the clock divider. The internal PWM counter will increments at
     * f_clk / 64
     *
     * where:
     *  f_clk = the frequency of the myRIO FPGA clock (40 MHz default)
     */
    Pwm_ClockSelect(&pwmA0, Pwm_64x);

    /*
     * Set the clock divider. The internal PWM counter will increments at
     * f_clk / 64
     *
     * where:
     *  f_clk = the frequency of the myRIO FPGA clock (40 MHz default)
     */
    Pwm_ClockSelect(&pwmA1, Pwm_64x);

    /*
     * Set the maximum counter value..
     *
     * The counter increments at 40 MHz / 64 = 625 kHz and the counter counts
     * from 0 to 1974. The frequency of the PWM waveform is 625 kHz / 1874
     * = 333 Hz.
     */
    Pwm_CounterMaximum(&pwmA0, 1874);

    /*
     * Set the maximum counter value..
     *
     * The counter increments at 40 MHz / 64 = 625 kHz and the counter counts
     * from 0 to 1974. The frequency of the PWM waveform is 625 kHz / 1874
     * = 333 Hz.
     */
    Pwm_CounterMaximum(&pwmA1, 1874);

    /*
     * Set the comparison counter value.
     *
     * The duty cycle is 724 / 1874 = 39%
     */


    Pwm_CounterCompare(&pwmA0, 724);


    /*
      * Set the comparison counter value.
      *
      * The duty cycle is 937 / 1874 = 50% (1500 useconds period on) wich will make the wheel to not move
      */

    Pwm_CounterCompare(&pwmA1, 937);
    /*
     * PWM outputs are on pins shared with other onboard devices. To output on
     * a physical pin, select the PWM on the appropriate SELECT register. See
     * the MUX example for simplified code to enable-disable onboard devices.
     *
     * Read the value of the SYSSELECTA register.
     */
    status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg1);
    MyRio_ReturnValueIfNotSuccess(status, status,
        "Could not read from the SYSSELECTA register!")

    /*
     * Set bit2 of the SYSSELECTA register to enable PWMA_0 functionality.
     * The functionality of the bit is specified in the documentation.
     */

    selectReg1 = selectReg1 | (1 << 2);

    /*
     * Write the updated value of the SYSSELECTA register.
     */
    status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg1);
    MyRio_ReturnValueIfNotSuccess(status, status,
        "Could not write to the SYSSELECTA register!")


    /*
     * PWM outputs are on pins shared with other onboard devices. To output on
     * a physical pin, select the PWM on the appropriate SELECT register. See
     * the MUX example for simplified code to enable-disable onboard devices.
     *
     * Read the value of the SYSSELECTA register.
     */
    status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg2);
    MyRio_ReturnValueIfNotSuccess(status, status,
        "Could not read from the SYSSELECTA register!")

    /*
     * Set bit3 of the SYSSELECTA register to enable PWMA_1 functionality.
     * The functionality of the bit is specified in the documentation.
     */

    selectReg2 = selectReg2 | (1 << 3);

    /*
     * Write the updated value of the SYSSELECTA register.
     */
    status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg2);
    MyRio_ReturnValueIfNotSuccess(status, status,
        "Could not write to the SYSSELECTA register!")





return status;
}
