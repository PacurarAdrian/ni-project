/*
 * Sensor.h
 *
 *  Created on: 19.07.2016
 *      Author: Adi
 */

#ifndef SENSOR_H_
#define SENSOR_H_

#include "MyRio.h"
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "DIO.h"

#if NiFpga_Cpp
extern "C" {
#endif


void sleepFor(int microseconds);
void waitFor(int microseconds);
unsigned long getMicroseconds();
unsigned long getRaw();
float getCm(float tc);
//int read();

#if NiFpga_Cpp
}
#endif


#endif /* SENSOR_H_ */
