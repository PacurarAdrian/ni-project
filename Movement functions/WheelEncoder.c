/*
 * WheelEncoder.c
 *
 *  Created on: Sep 1, 2015
 *      Author: So
 */


#include <stdio.h>
#include <time.h>

#include "WheelEncoder.h"
#include "MyRio.h"

extern NiFpga_Session myrio_session;

#if !defined(LoopDuration)
#define LoopDuration   60  /* How long to monitor the signal, in seconds */
#endif


#if !defined(LoopStep)
#define LoopStep        1   /* How long to monitor the signal, in seconds */
#endif

int initializeEncoders(MyRio_Encoder *encB0,MyRio_Encoder *encA0)
{
	NiFpga_Status status;

	uint8_t selectRegB;
	uint8_t selectRegA;


	 /*
	 * Initialize the encoder struct with registers from the FPGA personality.
	 */
	(*encB0).cnfg = ENCBCNFG;
	(*encB0).stat = ENCBSTAT;
	(*encB0).cntr = ENCBCNTR;

	(*encA0).cnfg = ENCACNFG;
	(*encA0).stat = ENCASTAT;
	(*encA0).cntr = ENCACNTR;

	/*
	 * Encoder inputs are on pins shared with other onboard devices. To input
	 * from a physical pin, select the encoder on the appropriate SELECT
	 * register.
	 *
	 * Read the value of the SYSSELECTB register.
	 */
	status = NiFpga_ReadU8(myrio_session, SYSSELECTB, &selectRegB);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not read from the SYSSELECTB register!");

   /*
	* Read the value of the SYSSELECTB register.
	*/
	status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectRegA);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not read from the SYSSELECTA register!");
	/*
	 * Set bit 5 of the SYSSELECTB register to enable ENCB functionality.
	 * The functionality of these bits is specified in the documentation.
	 */
	selectRegB = selectRegB | (1 << 5);
	/*
	 * Set bit 5 of the SYSSELECTA register to enable ENCA functionality.
	 * The functionality of these bits is specified in the documentation.
	 */
	selectRegA = selectRegA | (1 << 5);

	/*
	 * Write the updated value of the SYSSELECTB register.
	 */
	status = NiFpga_WriteU8(myrio_session, SYSSELECTB, selectRegB);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not write to the SYSSELECTB register!")
	/*
	 * Write the updated value of the SYSSELECTA register.
	 */
	status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectRegA);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not write to the SYSSELECTA register!")
	/*
	 * Enable the encoder and configure to read step and direction signals.
	 */
	Encoder_Configure(encB0, Encoder_Enable | Encoder_SignalMode,
			Encoder_Enabled | Encoder_StepDirection);
	/*
	 * Enable the encoder and configure to read step and direction signals.
	 */
	Encoder_Configure(encA0, Encoder_Enable | Encoder_SignalMode,
			Encoder_Enabled | Encoder_StepDirection);

	return status;
}

/**
 * Overview:
 * Demonstrates using the encoder. Reads a step and direction signal from the
 * encoder on connector B. Prints the values to the console.
 *
 * Instructions:
 * 1. Connect a step and direction signal to encoder pins on connector B.
 * 2. Run this program.
 *
 * Output:
 * The program reads the encoder for 60 s. Encoder values are written to the
 * console every 1 s.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */
int EncoderOn()
{
    NiFpga_Status status;

    MyRio_Encoder encA0;

    uint8_t selectReg;

    time_t currentTime;
    time_t printTime;
    time_t finalTime;

    uint32_t steps;
    const char* direction;

    printf("Encoder\n");

    /*
     * Initialize the encoder struct with registers from the FPGA personality.
     */
    encA0.cnfg = ENCBCNFG;
    encA0.stat = ENCBSTAT;
    encA0.cntr = ENCBCNTR;


    /*
     * Encoder inputs are on pins shared with other onboard devices. To input
     * from a physical pin, select the encoder on the appropriate SELECT
     * register.
     *
     * Read the value of the SYSSELECTB register.
     */
    status = NiFpga_ReadU8(myrio_session, SYSSELECTB, &selectReg);
    MyRio_ReturnValueIfNotSuccess(status, status,
        "Could not read from the SYSSELECTB register!");

    /*
     * Set bit 5 of the SYSSELECTB register to enable ENCB functionality.
     * The functionality of these bits is specified in the documentation.
     */
    selectReg = selectReg | (1 << 5);

    /*
     * Write the updated value of the SYSSELECTB register.
     */
    status = NiFpga_WriteU8(myrio_session, SYSSELECTB, selectReg);
    MyRio_ReturnValueIfNotSuccess(status, status,
        "Could not write to the SYSSELECTB register!")

    /*
     * Enable the encoder and configure to read step and direction signals.
     */
    Encoder_Configure(&encA0, Encoder_Enable | Encoder_SignalMode,
            Encoder_Enabled | Encoder_StepDirection);

    /*
     * Normally, the main function runs a long running or infinite loop.
     * Read the encoder output for 60 seconds.
     */
    time(&currentTime);
    finalTime = currentTime + LoopDuration;
    printTime = currentTime;
    while (currentTime < finalTime)
    {
        time(&currentTime);

        /* Don't print every loop iteration. */
        if (currentTime > printTime)
        {
            steps = Encoder_Counter(&encA0);
            if ((Encoder_Status(&encA0) & Encoder_StDirection)
                == Encoder_Incrementing)
            {
                direction = "incrementing";
            }
            else
            {
                direction = "decrementing";
            }

            printf("Steps %d, Direction: %s\n", steps, direction);

            printTime += LoopStep;
        }
    }



    /*
     * Returns 0 if successful.
     */
    return status;
}
