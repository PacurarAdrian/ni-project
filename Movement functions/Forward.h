/*
 * Forward.h
 *
 *  Created on: Sep 1, 2015
 *      Author: So
 */

#ifndef FORWARD_H_
#define FORWARD_H_

#include "MyRio.h"
#include "PWM.h"

#if NiFpga_Cpp
extern "C" {
#endif


int forward();
void initializePmw(MyRio_Pwm *pwmA2,MyRio_Pwm *pwmA1);
int setSpeedForward(MyRio_Pwm *pwmA2,MyRio_Pwm *pwmA1,int gear);
int setSpeedRight(MyRio_Pwm *pwmA2,int gear,int speed);
int setSpeedLeft(MyRio_Pwm *pwmA1,int gear,int speed);

#if NiFpga_Cpp
}
#endif


#endif /* FORWARD_H_ */
