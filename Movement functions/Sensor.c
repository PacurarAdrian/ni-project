/*
 * Sensor.c
 *
 *  Created on: 19.07.2016
 *      Author: Adi
 */



#include "Sensor.h"

#if !defined(LoopDuration)
#define LoopDuration    60  /* How long to output the signal, in seconds */
#endif

#if !defined(LoopSteps)
#define LoopSteps       1   /* How long to step between printing, in seconds */
#endif

/**
 * Overview:
 * Demonstrates using the digital input and output (DIO). Reads initial values
 * of two digital input channels from connector A and writes the Boolean AND of
 * the read values on connector B. Prints the values to the console.
 *
 * Instructions:
 * 1. Connect a DC voltage input (either 0 V or 5 V) to DIO 0 on connector A.
 * 2. Connect a DC voltage input (either 0 V or 5 V) to DIO 7 on connector A.
 * 3. Connect a voltmeter to DIO 7 on connector B.
 * 4. Run this program.
 *
 * Output:
 * The program reads the initial voltage on DI0 0 and DIO 7 on connector A, and
 * writes the Boolean AND on DIO 7 on connector B. for 60 s. The output is
 * maintained for 60 s. Input voltages and the output are written to the
 * console.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */

void sleepFor(int microseconds)
{
	struct timespec ts;
	ts.tv_sec = microseconds / 1000000;
	ts.tv_nsec = (microseconds % 1000000) * 1000;
	nanosleep(&ts, NULL);
}
unsigned long getMicroseconds()
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	unsigned long time_in_micros = 1000000 * tv.tv_sec + tv.tv_usec;
	return time_in_micros;
}

void waitFor(int microseconds)
{
	unsigned long currentTime;
	unsigned long finalTime;
	currentTime=getMicroseconds();
	finalTime = currentTime + microseconds;
	while (currentTime < finalTime)
	{
		currentTime=getMicroseconds();
	};
}
unsigned long getRaw()
{
	unsigned long startTime;
	unsigned long currentTime;
	unsigned long finalTime;
	NiFpga_Bool dio_B0= NiFpga_True;//write initial value of 1
	MyRio_Dio B0;

	/*
     * Specify the registers that correspond to the DIO channel
     * that needs to be accessed.
     */

	B0.dir = DIOB_70DIR;
	B0.out = DIOB_70OUT;
	B0.in = DIOB_70IN;
	B0.bit = 0;
	/*
	 * Print out the desired logic level of B/DIO0.
	 */
	printf("B/DIO0 = %d\n", dio_B0);
	/*
	 * Write to channel B/DIO0 to set it to the 1 value.
	 */
	Dio_WriteBit(&B0, dio_B0);
	//hold state for 5 microseconds
	waitFor(5);



	currentTime=getMicroseconds();
	dio_B0=NiFpga_False;//write value of 0; enter holdoff state
	/*
	 * Write to channel B/DIO0 to set it to the 0 value.
	 */
	Dio_WriteBit(&B0, dio_B0);

	finalTime = currentTime + 720; // wait 720 us; the rest 30 us are taken as margin of error
	while (currentTime < finalTime)
	{
		currentTime=getMicroseconds();
	};
	currentTime=getMicroseconds();
	finalTime = currentTime + 18.5*1000+30;// 18.5 ms + 30 us for the holdoff state
	NiFpga_Bool val,prevVal;//values of 2 consecutive readings
	prevVal=Dio_ReadBit(&B0);
	//startTime=currentTime;
	//printf("receiving input:\n");
	while (currentTime < finalTime)//wait for an input signal or duration proportional to the distance
	{
		 /*
		 * Read from DIO channels B/DIO0.
		 * channel is on the 8-channel bank on Connector B.
		 */
		dio_B0 = Dio_ReadBit(&B0);
		//printf("dio B0:%d\n",dio_B0);
		val=dio_B0;
		currentTime=getMicroseconds();
		if(prevVal==NiFpga_False && val==NiFpga_True)//register rising edge of signal
		{
			startTime=currentTime;

		}
		if(prevVal==NiFpga_True && val==NiFpga_False)//register falling edge and end loop
		{
			break;
		}
		prevVal=val;

	};
	if(val!=0)//write 0 value in preparation for the next reading
	{
		dio_B0=0;
		/*
		 * Write to channel B/DIO0 to set it to the 0 value.
		 */
		Dio_WriteBit(&B0, dio_B0);
	}

	return currentTime-startTime;
}

float getCm(float tc)
{
	//return (getRaw()*3/20);//raw*6,6667
	float cair=331.5+(0.6*tc);// m/s

	float sobject= (cair*getRaw())/20000;
	return sobject;
}
int getMm()
{
	return (getRaw()*3/2); //raw*0,6667
}
/*
int read()
{
    NiFpga_Status status;


    time_t currentTime;
    time_t finalTime,printTime;

    printf("Ping Sensor test\n");




    /*
     * Open the myRIO NiFpga Session.
     * This function MUST be called before all other functions. After this call
     * is complete the myRIO target will be ready to be used.

    status = MyRio_Open();
    if (MyRio_IsNotSuccess(status))
    {
        return status;
    }
*/

    /*
     * Normally, the main function runs a long running or infinite loop.
     * Keep the program running so that you can measure the input from B0 using
     * an external instrument.

    time(&currentTime);
    finalTime = currentTime + LoopDuration;
    printTime=currentTime;
    while (currentTime < finalTime)
    {
    	time(&currentTime);

    	if (currentTime > printTime)
    	{
    		printf("Raw result :%lu\n",getRaw());
    		printTime += LoopSteps;
    	}
    }

    /*
     * Close the myRIO NiFpga Session.
     * This function MUST be called after all other functions.
     */
    //status = MyRio_Close();

    /*
     * Returns 0 if successful.

    return status;
}*/
