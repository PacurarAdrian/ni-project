/*
 * Stop.h
 *
 *  Created on: Sep 2, 2015
 *      Author: So
 */

#ifndef STOP_H_
#define STOP_H_

#include "MyRio.h"

#if NiFpga_Cpp
extern "C" {
#endif


void stop();


#if NiFpga_Cpp
}
#endif


#endif /* STOP_H_ */
