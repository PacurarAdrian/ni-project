/*
 * Copyright (c) 2013,
 * National Instruments Corporation.
 * All rights reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <time.h>

#include "MyRio.h"
#include "WheelEncoder.h"
//#include "Backward.h"
#include "Forward.h"
//#include "StearRight.h"
//#include "StearLeft.h"
#include "Encoder.h"
//#include "PWM.h"
#include "SensorMotor.h"
#include "Sensor.h"


#if !defined(LoopDuration)
#define LoopDuration    600  /* How long to output the signal, in seconds */
#endif


#if !defined(LoopSteps)
#define LoopSteps       1   /* How long to step between printing, in seconds */
#endif


char command='5';
int speedLeft=0,speedRight=0;

void* getCommand(void* arg) {
	int* val = (int*) arg;
	printf("Thread with argument %d\n", *val);
	while(1)
	{
		scanf("%c\n",&command);
		//command=(char)getc(stdin);
	}
}
int min(int a,int b)
{
	if(a<=b)
		return a;
	else return b;
}


void reglateSpeed(int stepsLeft,int stepsRight,int gear)
{
	int dif=(abs(stepsLeft-stepsRight))/2;
	if(stepsLeft-stepsRight>2)
		{
			speedLeft=-min(dif,100)/gear;
			speedRight=+min(dif,100)/gear;
		}else if(stepsRight-stepsLeft>2)
		{
			speedLeft=+min(dif,100)/gear;
			speedRight=-min(dif,100)/gear;
		}
		else if(stepsLeft-stepsRight>0 && stepsLeft-stepsRight<=2)//decrease left or increase right
		{
			if(abs(speedLeft)>abs(speedRight))
			{
				if(speedLeft<0)//decrease left
					speedLeft++;
				else speedLeft--;
			}
			else {
				if(speedRight<0)//increase right
					speedRight--;
				else speedRight++;
			}
		}
		else if(stepsRight-stepsLeft>0 && stepsRight-stepsLeft<=2)//decrease right or increase left
			{
				if(abs(speedRight)>abs(speedLeft))
				{
					if(speedRight<0)//decrease right
						speedRight++;
					else speedRight--;
				}
				else {
					if(speedLeft<0)//increase left
						speedLeft--;
					else speedLeft++;
				}
			}
}

NiFpga_Status goForward(MyRio_Pwm pwmA2,MyRio_Pwm pwmA1,int gear,int stepsLeft,int stepsRight)
{
	NiFpga_Status status;

	reglateSpeed(stepsLeft,stepsRight,gear);

	status=setSpeedRight( &pwmA2,gear,speedRight);
	if(MyRio_IsNotSuccess(status))
	{
		printf("Error occurred while setting right motor to gear %d",gear);
		return status;
	}
	status=setSpeedLeft(&pwmA1,gear,speedLeft);
	if(MyRio_IsNotSuccess(status))
	{
		printf("Error occurred while setting left motor to gear %d",gear);
		return status;
	}
	printf("speed forward left:%d\nspeed forward right:%d\n",speedLeft,speedRight);
	return status;
}
NiFpga_Status goBackward(MyRio_Pwm pwmA2,MyRio_Pwm pwmA1,int gear,int stepsLeft,int stepsRight)
{
	NiFpga_Status status;

	reglateSpeed(stepsLeft,stepsRight,gear);

	status=setSpeedRight( &pwmA2,-gear,speedRight);
	if(MyRio_IsNotSuccess(status))
	{
		printf("Error occurred while setting right motor to gear %d",-gear);
		return status;
	}
	status=setSpeedLeft(&pwmA1,-gear,speedLeft);
	if(MyRio_IsNotSuccess(status))
	{
		printf("Error occurred while setting left motor to gear %d",-gear);
		return status;
	}
	printf("speed backward left:%d\nspeed backward right:%d\n",speedLeft,speedRight);
	return status;
}

int main(int argc, char **argv)
{
    NiFpga_Status status;

    time_t currentTime;
    time_t finalTime;
    time_t printTime;
    time_t startTime;
//encoder variables
    MyRio_Encoder encB0;
    MyRio_Encoder encA0;
    uint32_t stepsB;
	uint32_t stepsA;
	uint32_t stepsRefB;
	uint32_t stepsRefA;
	const char* directionA;
	const char* directionB;
//motor variables
	MyRio_Pwm pwmA2;
	MyRio_Pwm pwmA1;
	int gear=1;
//sensor motor variables
	int pozSensor=90;//sensor faces forward
//command thread variables
	pthread_t th1;
	int arg1 = 1;
	int code;
//sensor variables
	float temperature=22;

   //open thread for reading commands
	if((code=pthread_create(&th1, NULL, getCommand, &arg1))!=0)
	{
		printf("could not start thread");
		return code;
	}
	 /*
	 * Open the myRIO NiFpga Session.
	 * This function MUST be called before all other functions. After this call
	 * is complete the myRIO target will be ready to be used.
	 */
    status = MyRio_Open();
    if (MyRio_IsNotSuccess(status))
    {
        return status;
    }

    /*
     * Here is where you will need to write all the instructions for the robot(between
     * open and close opperations). When myRIO NiFpga Session is closed, all the pins
     * get the default value, so you will have to work with temporizations( see WheelEncoder
     * example).
     */
    status=initializeEncoders(&encB0,&encA0);
    if(MyRio_IsNotSuccess(status))
    {
	    return status;
    }


    initializePmw( &pwmA2, &pwmA1);


    status=position(pozSensor);
	if(MyRio_IsNotSuccess(status))
	{
		return status;
	}
	//read temperature
	//printf("enter temperature for sensor:");
	//scanf("%f",&temperature);
    /*
	 * Normally, the main function runs a long running or infinite loop.
	 * Keep the program running for 60 seconds so that the PWM output can be
	 * observed using an external instrument.
	 */
	time(&currentTime);
	startTime = currentTime;
	finalTime = currentTime + LoopDuration;
	printTime = currentTime;
	char prevCommand=command;
	while (currentTime < finalTime)
	{
		time(&currentTime);


		/* Don't print every loop iteration. */
		if (currentTime > printTime)
		{

			//printf("insert command: ");
			//scanf("%c\n",&command);
			//command=(char)getc(stdin);

			printf("command is :%c\n",command);
			switch(command)
			{
			case '8'://go forward
				if(prevCommand!= '8')
				{
					stepsRefA=stepsA;
					stepsRefB=stepsB;
					speedRight=0;
					speedLeft=0;
				}
				status=goForward(pwmA2,pwmA1,gear,stepsA-stepsRefA,stepsB-stepsRefB);
				printf("Left: Steps %d, Direction: %s\n", stepsA-stepsRefA, directionA);
				printf("Right: Steps %d, Direction: %s\n", stepsB-stepsRefB, directionB);
				printf("%g s: going forward\n",difftime(currentTime,startTime));
			break;
			case '2'://go backwards
				if(prevCommand!= '2')
				{
					stepsRefA=stepsA;
					stepsRefB=stepsB;
					speedRight=0;
					speedLeft=0;
				}
				status=goBackward(pwmA2,pwmA1,gear,stepsA-stepsRefA,stepsB-stepsRefB);
				printf("Left: Steps %d, Direction: %s\n", stepsA-stepsRefA, directionA);
				printf("Right: Steps %d, Direction: %s\n", stepsB-stepsRefB, directionB);
				printf("%g s: going backwards\n",difftime(currentTime,startTime));
				break;
			case '4'://steer left
				if(prevCommand!= '4')
				{
					stepsRefA=stepsA;
					stepsRefB=stepsB;
					speedRight=0;
					speedLeft=0;
				}
				status=setSpeedRight( &pwmA2,gear,0);
				if(MyRio_IsNotSuccess(status))
				{
					printf("Error occurred while setting right motor to gear %d",gear);
					return status;
				}
				status=setSpeedLeft(&pwmA1,0,0);
				if(MyRio_IsNotSuccess(status))
				{
					perror("Error occurred while stopping left motor");
					return status;
				}
				printf("Left: Steps %d, Direction: %s\n", stepsA-stepsRefA, directionA);
				printf("Right: Steps %d, Direction: %s\n", stepsB-stepsRefB, directionB);
				printf("%g s: steering left\n",difftime(currentTime,startTime));
				break;
			case '6'://steer right
				if(prevCommand!= '6')
				{
					stepsRefA=stepsA;
					stepsRefB=stepsB;
					speedRight=0;
					speedLeft=0;
				}
				status=setSpeedRight( &pwmA2,0,0);
				if(MyRio_IsNotSuccess(status))
				{
					perror("Error occurred while stopping right motor");
					return status;
				}
				status=setSpeedLeft(&pwmA1,gear,0);
				if(MyRio_IsNotSuccess(status))
				{
					printf("Error occurred while setting left motor to gear %d",gear);
					return status;
				}
				printf("Left: Steps %d, Direction: %s\n", stepsA-stepsRefA, directionA);
				printf("Right: Steps %d, Direction: %s\n", stepsB-stepsRefB, directionB);
				printf("%g : steering right\n",difftime(currentTime,startTime));
				break;

			case '.'://increase degree of sensor motor
				if(pozSensor<180)
					pozSensor+=10;
				status=position(pozSensor);
				if(MyRio_IsNotSuccess(status))
				{
					perror("Error occurred while moving sensor to right");
					return status;
				}
				printf(" %g s: moving sensor to the right at %d degrees\n",difftime(currentTime,startTime),pozSensor);
				break;
			case ','://decrease degree of sensor motor
				if(pozSensor>0)
					pozSensor-=10;
				status=position(pozSensor);
				if(MyRio_IsNotSuccess(status))
				{
					perror("Error occurred while moving sensor to left");
					return status;
				}
				printf("%g s: moving sensor to the left at %d degrees\n",difftime(currentTime,startTime),pozSensor);
				break;
			case '-'://decrease gear
				if(gear>-4)
					gear--;
				printf("%g s: decreasing gear to %d\n",difftime(currentTime,startTime),gear);
				break;
			case '+'://increase gear
				if(gear<4)
					gear++;
				printf("%g s: increasing gear to %d\n",difftime(currentTime,startTime),gear);
				break;
			case 'q'://=q ==exit loop
				currentTime = finalTime;
				//pthread_join(th1, NULL);
				break;
			case 's'://sensor read
				printf("%g : sensor raw reading: %lu\n",difftime(currentTime,startTime),getRaw());
				//printf("%g s: sensor reading: %f cm\n",difftime(currentTime,startTime),getCm(temperature));
				break;
			default ://stop
				status=setSpeedRight( &pwmA2,0,0);
				if(MyRio_IsNotSuccess(status))
				{
					perror("Error occurred while stopping right motor");
					return status;
				}
				status=setSpeedLeft(&pwmA1,0,0);
				if(MyRio_IsNotSuccess(status))
				{
					perror("Error occurred while stopping left motor");
					return status;
				}
				printf("%g s: stopping\n",difftime(currentTime,startTime));
				break;
			}

			if(command=='2'||command=='5'||command=='8'||command=='4'||command=='6')
			{
				prevCommand=command;
			}
			else {
				command=prevCommand;
			}
			printf("Robot in gear %d\n",gear);

			//read encoders
			stepsB = Encoder_Counter(&encB0);//right encoder
			if ((Encoder_Status(&encB0) & Encoder_StDirection)
				== Encoder_Incrementing)
			{
				directionB = "incrementing";
			}
			else
			{
				directionB = "decrementing";
			}


			stepsA = -Encoder_Counter(&encA0);//left encoder (minus because the motor is inverted)
			if ((Encoder_Status(&encA0) & Encoder_StDirection)
				== Encoder_Decrementing)//decrementing because the motor is inverted
			{
				directionA = "incrementing";
			}
			else
			{
				directionA = "decrementing";
			}



			printTime += LoopSteps;

		}

	}
	printf("Left:Total Steps %d, Direction: %s\n", stepsA, directionA);
	printf("Right:Total Steps %d, Direction: %s\n", stepsB, directionB);

    /*
     * Close the myRIO NiFpga Session.
     * This function MUST be called after all other functions.
     */

    status = MyRio_Close();
    return status;
}

