/*
 * Forward.c
 *
 *  Created on: Sep 1, 2015
 *      Author: So
 */

#include "Forward.h"
#include <stdio.h>

#include <time.h>

extern NiFpga_Session myrio_session;





void initializePmw(MyRio_Pwm *pwmA2,MyRio_Pwm *pwmA1)
{

	/*
	 * Initialize the PWM struct with registers from the FPGA personality.
	 */
	(*pwmA2).cnfg = PWMA_2CNFG;
	(*pwmA2).cs = PWMA_2CS;
	(*pwmA2).max = PWMA_2MAX;
	(*pwmA2).cmp = PWMA_2CMP;
	(*pwmA2).cntr = PWMA_2CNTR;


	/*
	* Initialize the PWM struct with registers from the FPGA personality.
	*/
   (*pwmA1).cnfg = PWMA_1CNFG;
   (*pwmA1).cs = PWMA_1CS;
   (*pwmA1).max = PWMA_1MAX;
   (*pwmA1).cmp = PWMA_1CMP;
   (*pwmA1).cntr = PWMA_1CNTR;



	/*
	 * Set the waveform, enabling the PWM onboard device.
	 */
	Pwm_Configure(pwmA2, Pwm_Invert | Pwm_Mode,
			Pwm_NotInverted | Pwm_Enabled);

	/*
	 * Set the waveform, enabling the PWM onboard device.
	 */
	Pwm_Configure(pwmA1, Pwm_Invert | Pwm_Mode,
			Pwm_NotInverted | Pwm_Enabled);

	/*
	 * Set the clock divider. The internal PWM counter will increments at
	 * f_clk / 64
	 *
	 * where:
	 *  f_clk = the frequency of the myRIO FPGA clock (40 MHz default)
	 */
	Pwm_ClockSelect(pwmA2, Pwm_64x);

	/*
	 * Set the clock divider. The internal PWM counter will increments at
	 * f_clk / 64
	 *
	 * where:
	 *  f_clk = the frequency of the myRIO FPGA clock (40 MHz default)
	 */
	Pwm_ClockSelect(pwmA1, Pwm_64x);

	/*
	 * Set the maximum counter value..
	 *
	 * The counter increments at 40 MHz / 64 = 625 kHz and the counter counts
	 * from 0 to 1974. The frequency of the PWM waveform is 625 kHz / 1874
	 * = 333 Hz.
	 */
	Pwm_CounterMaximum(pwmA2, 1874);

	/*
	 * Set the maximum counter value.
	 *
	 * The counter increments at 40 MHz / 64 = 625 kHz and the counter counts
	 * from 0 to 1974. The frequency of the PWM waveform is 625 kHz / 1874
	 * = 333 Hz.
	 */
	Pwm_CounterMaximum(pwmA1, 1874);
}

int setSpeedForward(MyRio_Pwm *pwmA2,MyRio_Pwm *pwmA1,int gear)
{
	uint16_t speedRight[5]={ 967,1067,1167,1267,1367};
	uint16_t speedLeft[5]={ 937,837,737,637,537};
	NiFpga_Status status;

	uint8_t selectReg1;
	uint8_t selectReg2;
	/*
	 * Set the comparison counter value.
	 *
	 * The duty cycle is 824/ 1874 = 44%.
	 */

	Pwm_CounterCompare(pwmA2, speedRight[gear]);


	/*
	 * Set the comparison counter value.
	 *
	 * The duty cycle is 1062 / 1874 = 56%.
	 */

	Pwm_CounterCompare(pwmA1,speedLeft[gear]);
	/*
	 * PWM outputs are on pins shared with other onboard devices. To output on
	 * a physical pin, select the PWM on the appropriate SELECT register. See
	 * the MUX example for simplified code to enable-disable onboard devices.
	 *
	 * Read the value of the SYSSELECTA register.
	 */
	status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg1);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not read from the SYSSELECTA register!")

	/*
	 * Set bit4 of the SYSSELECTA register to enable PWMA_2 functionality.
	 * The functionality of the bit is specified in the documentation.
	 */

	selectReg1 = selectReg1 | (1 << 4);

	/*
	 * Write the updated value of the SYSSELECTA register.
	 */
	status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg1);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not write to the SYSSELECTA register!")


	/*
	 * PWM outputs are on pins shared with other onboard devices. To output on
	 * a physical pin, select the PWM on the appropriate SELECT register. See
	 * the MUX example for simplified code to enable-disable onboard devices.
	 *
	 * Read the value of the SYSSELECTA register.
	 */
	status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg2);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not read from the SYSSELECTA register!")

	/*
	 * Set bit 3 of the SYSSELECTA register to enable PWMA_1 functionality.
	 * The functionality of the bit is specified in the documentation.
	 */

	selectReg2 = selectReg2 | (1 << 3);

	/*
	 * Write the updated value of the SYSSELECTA register.
	 */
	status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg2);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not write to the SYSSELECTA register!")



	return status;
}

int setSpeedLeft(MyRio_Pwm *pwmA1,int gear,int speed)
{
	uint16_t speedLeft[9]={1337,1237,1137,1067, 937,837,737,637,537};
	NiFpga_Status status;


	uint8_t selectReg2;

	gear+=4;
	speed=speed%100;
	/*
	 * Set the comparison counter value.
	 *
	 * The duty cycle is 1062 / 1874 = 56%.
	 */

	Pwm_CounterCompare(pwmA1,speedLeft[gear]-speed);



	/*
	 * PWM outputs are on pins shared with other onboard devices. To output on
	 * a physical pin, select the PWM on the appropriate SELECT register. See
	 * the MUX example for simplified code to enable-disable onboard devices.
	 *
	 * Read the value of the SYSSELECTA register.
	 */
	status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg2);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not read from the SYSSELECTA register!")

	/*
	 * Set bit3 of the SYSSELECTA register to enable PWMA_1 functionality.
	 * The functionality of the bit is specified in the documentation.
	 */

	selectReg2 = selectReg2 | (1 << 3);

	/*
	 * Write the updated value of the SYSSELECTA register.
	 */
	status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg2);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not write to the SYSSELECTA register!")



	return status;
}


int setSpeedRight(MyRio_Pwm *pwmA2,int gear,int speed)
{
	uint16_t speedRight[9]={567,667,767,837, 967,1067,1167,1267,1367};

	NiFpga_Status status;

	uint8_t selectReg1;
	gear+=4;

	/*
	 * Set the comparison counter value.
	 *
	 * The duty cycle is 824/ 1874 = 44%.
	 */
	speed=speed%100;
	Pwm_CounterCompare(pwmA2, speedRight[gear]+speed);


	/*
	 * PWM outputs are on pins shared with other onboard devices. To output on
	 * a physical pin, select the PWM on the appropriate SELECT register. See
	 * the MUX example for simplified code to enable-disable onboard devices.
	 *
	 * Read the value of the SYSSELECTA register.
	 */
	status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg1);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not read from the SYSSELECTA register!")

	/*
	 * Set bit4 of the SYSSELECTA register to enable PWMA_2 functionality.
	 * The functionality of the bit is specified in the documentation.
	 */

	selectReg1 = selectReg1 | (1 << 4);

	/*
	 * Write the updated value of the SYSSELECTA register.
	 */
	status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg1);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not write to the SYSSELECTA register!")

	return status;
}

/**
 * Overview:
 * Demonstrates moving the robot forward.
 *
 * Instructions:
 * 1. Connect the signal wire of the left motor to Connector A, PWM1.
 * 2. Connect the signal wire of the right motor to Connector A, PWM2.
 * 2. Run this program.
 *
 * Output:
 * The program generates two PWM signals that will move the robot forward.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */
int forward()
{
    NiFpga_Status status;



	MyRio_Pwm pwmA2;
	MyRio_Pwm pwmA1;

	uint8_t selectReg1;
	uint8_t selectReg2;



	printf("Forward\n");

	/*
	 * Initialize the PWM struct with registers from the FPGA personality.
	 */
	pwmA2.cnfg = PWMA_2CNFG;
	pwmA2.cs = PWMA_2CS;
	pwmA2.max = PWMA_2MAX;
	pwmA2.cmp = PWMA_2CMP;
	pwmA2.cntr = PWMA_2CNTR;


	/*
	* Initialize the PWM struct with registers from the FPGA personality.
	*/
   pwmA1.cnfg = PWMA_1CNFG;
   pwmA1.cs = PWMA_1CS;
   pwmA1.max = PWMA_1MAX;
   pwmA1.cmp = PWMA_1CMP;
   pwmA1.cntr = PWMA_1CNTR;



	/*
	 * Set the waveform, enabling the PWM onboard device.
	 */
	Pwm_Configure(&pwmA2, Pwm_Invert | Pwm_Mode,
			Pwm_NotInverted | Pwm_Enabled);

	/*
	 * Set the waveform, enabling the PWM onboard device.
	 */
	Pwm_Configure(&pwmA1, Pwm_Invert | Pwm_Mode,
			Pwm_NotInverted | Pwm_Enabled);

	/*
	 * Set the clock divider. The internal PWM counter will increments at
	 * f_clk / 64
	 *
	 * where:
	 *  f_clk = the frequency of the myRIO FPGA clock (40 MHz default)
	 */
	Pwm_ClockSelect(&pwmA2, Pwm_64x);

	/*
	 * Set the clock divider. The internal PWM counter will increments at
	 * f_clk / 64
	 *
	 * where:
	 *  f_clk = the frequency of the myRIO FPGA clock (40 MHz default)
	 */
	Pwm_ClockSelect(&pwmA1, Pwm_64x);

	/*
	 * Set the maximum counter value..
	 *
	 * The counter increments at 40 MHz / 64 = 625 kHz and the counter counts
	 * from 0 to 1974. The frequency of the PWM waveform is 625 kHz / 1874
	 * = 333 Hz.
	 */
	Pwm_CounterMaximum(&pwmA2, 1874);

	/*
	 * Set the maximum counter value.
	 *
	 * The counter increments at 40 MHz / 64 = 625 kHz and the counter counts
	 * from 0 to 1974. The frequency of the PWM waveform is 625 kHz / 1874
	 * = 333 Hz.
	 */
	Pwm_CounterMaximum(&pwmA1, 1874);

	/*
	 * Set the comparison counter value.
	 *
	 * The duty cycle is 824/ 1874 = 44%.
	 */

	Pwm_CounterCompare(&pwmA2, 1067);


	/*
	 * Set the comparison counter value.
	 *
	 * The duty cycle is 1062 / 1874 = 56%.
	 */

	Pwm_CounterCompare(&pwmA1,837);
	/*
	 * PWM outputs are on pins shared with other onboard devices. To output on
	 * a physical pin, select the PWM on the appropriate SELECT register. See
	 * the MUX example for simplified code to enable-disable onboard devices.
	 *
	 * Read the value of the SYSSELECTA register.
	 */
	status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg1);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not read from the SYSSELECTA register!")

	/*
	 * Set bit4 of the SYSSELECTA register to enable PWMA_2 functionality.
	 * The functionality of the bit is specified in the documentation.
	 */

	selectReg1 = selectReg1 | (1 << 4);

	/*
	 * Write the updated value of the SYSSELECTA register.
	 */
	status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg1);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not write to the SYSSELECTA register!")


	/*
	 * PWM outputs are on pins shared with other onboard devices. To output on
	 * a physical pin, select the PWM on the appropriate SELECT register. See
	 * the MUX example for simplified code to enable-disable onboard devices.
	 *
	 * Read the value of the SYSSELECTA register.
	 */
	status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg2);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not read from the SYSSELECTA register!")

	/*
	 * Set bit3 of the SYSSELECTA register to enable PWMA_1 functionality.
	 * The functionality of the bit is specified in the documentation.
	 */

	selectReg2 = selectReg2 | (1 << 3);

	/*
	 * Write the updated value of the SYSSELECTA register.
	 */
	status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg2);
	MyRio_ReturnValueIfNotSuccess(status, status,
		"Could not write to the SYSSELECTA register!")



    return status;
}
