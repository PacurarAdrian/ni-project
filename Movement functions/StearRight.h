/*
 * StearRight.h
 *
 *  Created on: Sep 4, 2015
 *      Author: So
 */

#ifndef STEARRIGHT_H_
#define STEARRIGHT_H_

#include "MyRio.h"

#if NiFpga_Cpp
extern "C" {
#endif


void StearRight();


#if NiFpga_Cpp
}
#endif


#endif /* STEARRIGHT_H_ */
