/*
 * WheelEncoder.h
 *
 *  Created on: Sep 1, 2015
 *      Author: So
 */

#ifndef WHEELENCODER_H_
#define WHEELENCODER_H_

#include "MyRio.h"
#include "Encoder.h"

#if NiFpga_Cpp
extern "C" {
#endif


int EncoderON();
int initializeEncoders(MyRio_Encoder *,MyRio_Encoder *);

#if NiFpga_Cpp
}
#endif



#endif /* WHEELENCODER_H_ */
