/*
 * Backward.h
 *
 *  Created on: 13.07.2016
 *      Author: Adi
 */

#ifndef BACKWARD_H_
#define BACKWARD_H_

#include "MyRio.h"

#if NiFpga_Cpp
extern "C" {
#endif


void backward();


#if NiFpga_Cpp
}
#endif



#endif /* BACKWARD_H_ */
