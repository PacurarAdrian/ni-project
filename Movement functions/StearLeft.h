/*
 * StearLeft.h
 *
 *  Created on: Sep 1, 2015
 *      Author: So
 */
#ifndef STEARLEFT_H_
#define STEARLEFT_H_

#include "MyRio.h"

#if NiFpga_Cpp
extern "C" {
#endif


void StearLeft();


#if NiFpga_Cpp
}
#endif


#endif /* STEARLEFT_H_ */
