/*
 * Copyright (c) 2013,
 * National Instruments Corporation.
 * All rights reserved.
 */

#include <stdio.h>
#include <stdbool.h>
#include "MyRio.h"
#include <nivision.h>
#include <NIIMAQdx.h>

#define CAM_NAME    "cam1"
#define CAPTURED_IMAGE_PATH     "./capturedImage2.png"

#define WINDOW_NUMBER    15    // Image Display Window Number
#define IMAQDX_ERROR_MESSAGE_LENGTH    256

bool Log_Vision_Error(int errorValue);
bool Log_Imaqdx_Error(IMAQdxError errorValue);

/**
 * Overview:
 * myRIO main function. This template contains basic code to open the myRIO
 * FPGA session. You can remove this code if you only need to use the UART.
 *
 * Code in myRIO example projects is designed for reuse. You can copy source
 * files from the example project to use in this template.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */
int main(int argc, char **argv)
{
#if 1
    NiFpga_Status status;

    printf("\n ++ ImageAcquisitionAndDisplayExample \n");

    	Image* captureImage = NULL;
    	IMAQdxSession session = 0;
    /*
     * Open the myRIO NiFpga Session.
     * This function MUST be called before all other functions. After this call
     * is complete the myRIO target will be ready to be used.
     */
    status = MyRio_Open();
    if (MyRio_IsNotSuccess(status))
    {
        return status;
    }

    // Create the Image Buffer
    	captureImage = imaqCreateImage(IMAQ_IMAGE_U8, 0);

    	// Open a session to the selected camera
    	if (Log_Imaqdx_Error(IMAQdxOpenCamera(CAM_NAME, IMAQdxCameraControlModeController, &session)))
    		goto cleanup;

    	// Acquire an image
    	if (Log_Imaqdx_Error(IMAQdxSnap(session, captureImage)))
    		goto cleanup;

    	// Process the Captured image here.  For now we are simply displaying or logging
    	// the image to file.

    	//Display the Captured Image (on x64 Target)
    #if defined(__x86_64__)

    	RTDisplayVideoMode displayMode = IMAQ_GRAPHICS_MODE;

    	// Set the Display mode to Video
    	if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
    		goto cleanup;
    	//Display the Image
    	if(Log_Vision_Error (imaqRTDisplayImage(captureImage, WINDOW_NUMBER, 0)))
    		goto cleanup;

    	printf("\n Press any key to Continue... \n");
    	getchar(); // Wait for user input before the image unload

    	// Set the Display mode back to Text
    	displayMode = IMAQ_TEXT_MODE;
    	if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
    			goto cleanup;
    #else
    	// Write the captured image to the file
    	Log_Vision_Error(imaqWriteFile(captureImage, CAPTURED_IMAGE_PATH, NULL));

    #endif

    cleanup:
    	//Close the Camera session
    	IMAQdxCloseCamera (session);

    	// Dispose the image
    	imaqDispose(captureImage);

    	printf("\n -- ImageAcquisitionAndDisplayExample \n");
    	return 0;


    /*
     * Your application code goes here.
     */

    /*
     * Close the myRIO NiFpga Session.
     * This function MUST be called after all other functions.
     */
    status = MyRio_Close();
#endif
}

    bool Log_Imaqdx_Error(IMAQdxError errorValue)
    {
    	if (errorValue) {
    		char errorText[IMAQDX_ERROR_MESSAGE_LENGTH];
    		IMAQdxGetErrorString(errorValue, errorText, IMAQDX_ERROR_MESSAGE_LENGTH);
    		printf("\n %s ", errorText);
    		return true;
    	}
    	return false;
    }

    // Print the VISION Error Message
    bool Log_Vision_Error(int errorValue)
    {
    	if ( (errorValue != TRUE) && (imaqGetLastError() != ERR_SUCCESS)) {
    		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
    		printf("\n %s ", tempErrorText);
    		imaqDispose(tempErrorText);
    		return true;
    	}
    	return false;
    }

