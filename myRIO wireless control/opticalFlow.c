/*
 * opticalFlow.c
 *
 *  Created on: 14.03.2017
 *      Author: Adi
 */

#include "opticalFlow.h"
#include "MyRio.h"
#include "float.h"
#include "math.h"
#include <stdio.h>

#include <stdlib.h>
//#include <sys/time.h>
//#include <time.h>
#include <pthread.h>
#include "functions.h"



#define WINDOW_NUMBER    15    // Image Display Window Number
#define IMAQDX_ERROR_MESSAGE_LENGTH    256

// Print the IMAQDX Error Message
bool Log_Imaqdx_Error(IMAQdxError errorValue)
{
	if (errorValue) {
		char errorText[IMAQDX_ERROR_MESSAGE_LENGTH];
		IMAQdxGetErrorString(errorValue, errorText, IMAQDX_ERROR_MESSAGE_LENGTH);
		printf("\n %s ", errorText);
		return true;
	}
	return false;
}

// Print the VISION Error Message
bool Log_Vision_Error(int errorValue)
{
	if ( (errorValue != TRUE) && (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\n %s ", tempErrorText);
		imaqDispose(tempErrorText);
		return true;
	}
	return false;
}

/*
 * apply convolution kernel on the vector lpSrc at the position (i,j)
 *
 * @return    S  [lpSrc(i+k,j+l)*mask(k,l)]
 *           k,l
 * @params
 * - lpSrc = vector of unsigned chars, representing pixels of gray scale image
 * - w 		= width of image represented by lpSrc
 * - i 		= y position in the image represented by lpSrc
 * - j		= x position in the image represented by lpSrc
 * - mask	= convolution kernel
 * - maskSize= height and width of kernel matrix
 */
int applyMask(uint8_t *lpSrc,int w,int i,int j,short *mask[],int maskSize)
{
	int accm = 0;
	int half = maskSize / 2;
	int offset = (maskSize%2==0)? 1:0;
	int k,l;
	for (k = -half+offset; k < half+1; k++)
		for (l = -half+offset; l < half+1; l++)
		{
			accm += lpSrc[(i+k)*w + j+l]*mask[k+half-offset][l+half-offset];

		}
	return accm;
}

/*apply laplacian convolution kernel on the vector lpSrc at the position (i,j)
 *
* @return    S  [lpSrc(i+k,j+l)*mask(k,l)]
*           k,l
* @params
* - lpSrc = vector of unsigned chars, representing pixels of gray scale image
* - w 		= width of image represented by lpSrc
* - i 		= y position in the image represented by lpSrc
* - j		= x position in the image represented by lpSrc
* - mask	= convolution kernel
*/
float applyLaplace(float *lpSrc, int w, int i, int j,const float mask[3][3])
{
	float accm = 0;
	int k,l;
	for (k = -1; k < 2; k++)
		for (l = -1; l < 2; l++)
		{
			accm += lpSrc[(i + k)*w + j + l] * mask[k+1][l+1];

		}
	return accm;
}
//derivate images with displacement
/*
 * compute image intensity derivative with displacement from previous computed optical flow from images img1, img2
 *  using derivative masks mask1,mask2 and optical flow vectors disU and disV
 *  @params
 *  -img1,img2 = images form which derivatives are computed, img1 is the first image, img2 is the second
 *  			img1 and img2 must be gray scale images
 *  - mask1, mask2= convolution kernels used for computing derivative.
 *  				mask1 is applied to first image, mask2 is applied to second image
 *  - pRez = vector which must be declared externally, it will be populated with
 *  		the values of intensity derivative
 * - thresh : threshold used when computing derivative of images :
 * 					if the difference between the result of applying the convolution kernel to a
 * 					 position in the first image and the result of applying the convolution kernel
 * 					 to the same position in the second image is < threshold then in the image derivative
 * 					 only the first result is taken into consideration
 *  			it is used to lower noise
 * - step : used to shrink the images. if step=1 this function will apply convolution kernels to
 * 			every pixel in the images , if step=2 they will apply convolution kernels by skipping
 * 			one pixel (vertically and horizontally), if step=3 skip 2 pixels and so on.
 *			when step =1, the rezult image and derivation images are equal in width and height
 *			when step =2, they are halved in width and size;
 *			...
 * - disU, disV = optical flow vectors u=dx/dt and v=dy/dt
 *	@return - an image corresponding to the intensity derivative of images
 *			- grayscale image
 *			- NULL is returned when error occurs, message with error will be printed on screen
 */
Image* displacementDerivate(Image* img1, Image* img2, short *mask1[], short *mask2[], float *pRez,int maskSize ,float thresh,int step,float *disU,float *disV)
{
	/*
	int height = img1.rows;
	int width = img1.cols;
	int w = img1.step;
	uchar *lpSrc = img1.data;
	uchar *lpSrc2 = img2.data;
	Mat rez = Mat(height/step, width/step, CV_8UC1);


	int wr = rez.step;
	uchar *lpRez = rez.data;
	int half = maskSize / 2;
	*/
	int height;
		int width;
		int w;
	Image* rez=NULL;
		if((rez = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
			goto errExit;
		//if (Log_Imaqdx_Error(imaqGetImageSize(img1, &width, &height)))
		//		return -1;

		uint8_t *lpSrc = imaqImageToArray(img1, IMAQ_NO_RECT, &width, &height);
		uint8_t *lpSrc2 = imaqImageToArray(img2, IMAQ_NO_RECT, &width, &height);
		uint8_t *lpRez = (uint8_t*)malloc((height/step)*(width/step)*sizeof(uint8_t));
		//ImageInfo info;

		//imaqGetImageInfo( img1, &info );
		if(lpSrc==NULL)
		{
			imaqDispose(rez);
			goto errExit;
		}
		if(lpSrc2==NULL)
		{
			imaqDispose(lpSrc);
			imaqDispose(rez);
			goto errExit;
		}
		if(lpRez==NULL)
		{
			imaqDispose(lpSrc);
			imaqDispose(lpSrc2);
			imaqDispose(rez);
			goto errExit;
		}
		w=width;
		int wr=width/step;
		int i,j;
		int half = maskSize / 2;

	for (i = 0; i<height; i+=step)
		for (j = 0; j < width; j+=step) {
			if (i < half || i >= height - half || j < half || j >= width - half)
			{
				pRez[i/step*wr + j/step] = 0;

			}else
			{
				int x = max(half, min(width-half, j + (int)disU[i*w + j]));
				int y = max(half, min(height-half, i + (int)disV[i*w + j]));
				float accm1 =(float) applyMask(lpSrc,w,i,j, mask1,maskSize);
				x = max(half, min(width-half, j + (int)disU[i*w + j]));
				y = max(half, min(height-half, i + (int)disV[i*w + j]));
				float accm2=(float)applyMask(lpSrc2,w,y,x, mask2,maskSize);//apply displacement for pyramids
				if (abs(accm1 - accm2) < thresh)
					pRez[i/step*wr + j/step] = 2*accm1/(maskSize*maskSize);
				else pRez[i/step*wr + j/step] = (accm1+accm2)/(maskSize*maskSize);
			}

			lpRez[i/step*wr + j/step] = max(0, min(255, (int)pRez[i/step*wr + j/step]));
	}
	if (imaqArrayToImage(rez, lpRez, width/step,height/step)!=TRUE)
	{
		imaqDispose(lpSrc);
		imaqDispose(lpSrc2);
		imaqDispose(rez);
		free(lpRez);
		goto errExit;
	}
	//printf("disposing pointers");
	imaqDispose(lpSrc);
	imaqDispose(lpSrc2);
	free(lpRez);//------------------------------------------------------------------
	//printf("exiting derivate\n");


	return rez;
	errExit:
		if ( (imaqGetLastError() != ERR_SUCCESS)) {
			char *tempErrorText = imaqGetErrorText(imaqGetLastError());
			printf("\nerror in derivate with displacement: %s ", tempErrorText);
			imaqDispose(tempErrorText);

		}

		return NULL;
}
/*
 * compute image intensity derivative from images img1, img2
 *  using derivative masks mask1,mask2
 *  @params
 *  -img1,img2 = images form which derivatives are computed, img1 is the first image, img2 is the second
 *  			img1 and img2 must be gray scale images
 *  - mask1, mask2= convolution kernels used for computing derivative.
 *  				mask1 is applied to first image, mask2 is applied to second image
 *  - pRez = vector which must be declared externally, it will be populated with
 *  		the values of intensity derivative
 * - thresh : threshold used when computing derivative of images :
 * 					if the difference between the result of applying the convolution kernel to a
 * 					 position in the first image and the result of applying the convolution kernel
 * 					 to the same position in the second image is < threshold then in the image derivative
 * 					 only the first result is taken into consideration
 *  			it is used to lower noise
 * - step : used to shrink the images. if step=1 this function will apply convolution kernels to
 * 			every pixel in the images , if step=2 they will apply convolution kernels by skipping
 * 			one pixel (vertically and horizontally), if step=3 skip 2 pixels and so on.
 *			when step =1, the rezult image and derivation images are equal in width and height
 *			when step =2, they are halved in width and size;
 *			...
 *
 *	@return - an image corresponding to the intensity derivative of images
 *			- grayscale image
 *			- NULL is returned when error occurs, message with error will be printed on screen
 */
Image* derivate(Image *img1, Image *img2, short *mask1[], short *mask2[], float *pRez,int maskSize ,float thresh,int step)
{
	//printf("enter in derivate\n");
	int height;
	int width;
	int w;

	Image* flowmat=NULL;
	if((flowmat = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
		goto errExit;
	//if (Log_Imaqdx_Error(imaqGetImageSize(img1, &width, &height)))
	//		return -1;

	uint8_t *lpSrc = imaqImageToArray(img1, IMAQ_NO_RECT, &width, &height);
	uint8_t *lpSrc2 = imaqImageToArray(img2, IMAQ_NO_RECT, &width, &height);
	uint8_t *lpRez = (uint8_t*)malloc((height/step)*(width/step)*sizeof(uint8_t));
	//ImageInfo info;

	//imaqGetImageInfo( img1, &info );
	if(lpSrc==NULL)
	{
		imaqDispose(flowmat);
		goto errExit;
	}
	if(lpSrc2==NULL)
	{
		imaqDispose(lpSrc);
		imaqDispose(flowmat);
		goto errExit;
	}
	if(lpRez==NULL)
	{
		imaqDispose(lpSrc);
		imaqDispose(lpSrc2);
		imaqDispose(flowmat);
		goto errExit;
	}
	w=width;
	int wr=width/step;
	int i,j;
	int half = maskSize / 2;
	//unsigned char *pixel = info.imageStart;

	for (i = 0; i<height; i+=step)
	{
		for (j = 0; j < width; j+=step) {
			if (i < half || i >= height - half || j < half || j >= width - half)
			{
				pRez[i/step*wr + j/step] = 0;

			}else
			{
				float accm1 = (float) applyMask(lpSrc,w,i,j, mask1,maskSize);
				float accm2= (float)applyMask(lpSrc2,w,i,j, mask2,maskSize);
				if (fabs(accm1 - accm2) < thresh)
					pRez[i/step*wr + j/step] = 2*accm1/(maskSize*maskSize);
				else pRez[i/step*wr + j/step] = (accm1+accm2)/(maskSize*maskSize);
			}
			lpRez[i/step*wr + j/step] = max(0, min(255, pRez[i/step*wr + j/step]));
			//pixel = (unsigned char *)info.imageStart + i * info.pixelsPerLine + j;


		}

		//pixel += info.pixelsPerLine - info.xRes; // jump over all padding and border
	}


	if (imaqArrayToImage(flowmat, lpRez, width/step,height/step)!=TRUE)
	{
		imaqDispose(lpSrc);
		imaqDispose(lpSrc2);
		imaqDispose(flowmat);
		free(lpRez);
		goto errExit;
	}
//printf("disposing pointers");
	imaqDispose(lpSrc);
	imaqDispose(lpSrc2);
	free(lpRez);//------------------------------------------------------------------
	//printf("exiting derivate\n");
	return flowmat;
errExit:
	if ( (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\nerror in derivate %s ", tempErrorText);
		imaqDispose(tempErrorText);

	}

	return NULL;
}

/*encode optical flow represented by x,y in a RGB color
* 		- x 	= x component of image velocity or optical flow for given pixel in the image
 * 		- y		= y component of image velocity or optical flow for given pixel in the image
 * 		- umax 	= maximum value of the x component on image velocity vector
 * 		- umin	= minimum value of the x component on image velocity vector
 * 		- vmin 	= minimum value of the y component on image velocity vector
 * 		- vmax 	= maximum value of the y component on image velocity vector
 * 		- luminosity: is the Value in HSV (hue saturation value color space), real value in [0,1]
*/
RGBValue flowVecToRGB(float x, float y, float umin, float umax,float vmax,float vmin,float luminosity)
{
	RGBValue color;

	double sat = sqrt(y*y + x*x);
	double hue;
	if (y < 0)//c3 || C4
	{
		hue = 180 + 180 + atan2(y, x) * 180.0 / PI;// because atan2 has negative signs when y<0 another 180 degrees are added
	}
	else hue = atan2(y, x) * 180.0 / PI;
	double um = max(fabs(umax), fabs(umin));
	double vm = max(fabs(vmin), fabs(vmax));
	color = HSVToRGBA(hue, sat / sqrt(um*um + vm*vm),luminosity);
	return color;
}

//encode optical flow represented by u,v in an character
char getFlowChar2(float x,float y,float umin, float umax,float vmax,float vmin,float thresh)
{

	//printf("\narrows:\x18\x19\x1a\x1b\n");//sus jos dr stg
	double um = max(fabs(umax), fabs(umin));
	double vm = max(fabs(vmin), fabs(vmax));
	double sat = sqrt(y*y + x*x)/ sqrt(um*um + vm*vm);
	double hue;
	if (y < 0)//c3 || C4
		{
			hue = 180 + 180 + atan2(y, x) * 180.0 / PI;// because atan2 has negative signs when y<0 another 180 degrees are added
		}
		else hue = atan2(y, x) * 180.0 / PI;

	if (abs(sat)<thresh)///no movement detected
		return'.';
	else if (hue>=67.5&&hue<=112.5)//up
		return '\x18';//'^';
	else if (hue>=247.5&&hue<=292.5)//down
			return '\x19';//'_';
	else if (hue<=22.5&&hue>=337.5)//right
			return '\x1a';//'>';
	else if (hue>=157.5&&hue<=202.5)//left
			return '\x1b';//'<';
	else if (hue>202.5&&hue<247.5)//down left
			return '/';
	else if (hue>292.5&&hue<337.5)//down right
		return '\\';
	else if (hue>22.5&&hue<67.5)//up right
		return ')';
	else if (hue<157.5&&hue>112.5)//up left
		return '(';

	return '?';//something went wrong
}


/*
 * snap RGB image, convert to grayscale then save it to scrP
 */
void* captureImage(void *srcP)
{
	//printf("entering fct\n");
	//Image *src = (Image*)srcP;

	Image* srcC=NULL;

	IMAQdxSession session = 0;

	//printf("Create the Image Buffer\n");

	//src = imaqCreateImage(IMAQ_IMAGE_U8, 0);
	if((srcC = imaqCreateImage(IMAQ_IMAGE_RGB, 0))==NULL)
		goto cleanup;
	// Open a session to the selected camera
	if (Log_Imaqdx_Error(IMAQdxOpenCamera(CAM_NAME, IMAQdxCameraControlModeController, &session)))
	{
		fprintf(stderr,"\nerror in function capture image\ncould not open camera\n");
		// Dispose the image
		imaqDispose(srcC);
		goto exit;
	}

//printf("Acquire an image\n");
	if (Log_Imaqdx_Error(IMAQdxSnap(session, srcC)))
	{
		fprintf(stderr,"\nerror in function capture image\ncould not capture image\n");
		// Dispose the image
		imaqDispose(srcC);
		//Close the Camera session
		IMAQdxCloseCamera (session);
		goto exit;
	}

	//printf("convert to grayscale\n");
	if(imaqExtractColorPlanes(srcC,IMAQ_HSL,NULL,NULL,(Image*)srcP)!=TRUE)
	{
		fprintf(stderr,"\nerror in function capture image\ncould not convert to grayscale\n");
		//Close the Camera session
		IMAQdxCloseCamera (session);

		// Dispose the image
		imaqDispose(srcC);
		goto cleanup;
	}

	//Close the Camera session
	IMAQdxCloseCamera (session);

	// Dispose the image
	imaqDispose(srcC);
goto exit;
cleanup:
	if ( (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\nERROR: %s ", tempErrorText);
		imaqDispose(tempErrorText);
		srcP=NULL;
	}

exit:

	pthread_exit(NULL);
}

/*
 * Convert given HSV (hue saturation value color space) color to
 * RGBA(red green blue alpha color space) color
 * @params:
 * 	- h = HUE is in range [0-360] degrees
 * 	- s = Saturation in range [0-1] real number
 * 	- v = value in range [0-1] real number
 * 	@return:
 * 	- RGBValue structure containing equivalent color
 */
RGBValue HSVToRGBA(double h,double s,double v)
{
	double c = v*s; // find chroma
	double hp = h / 60.0;

	//int d = (int)hp / 2;
	//d *= 2;
	//double hpm = hp - d;// compute mod(hp)

	double x = c*(1 - fabs(fmod(hp, 2) - 1));
	double r, g, b;
	RGBValue rgb;
	if (0.0 <= hp && hp <= 1.0)
	{
		r = c;
		g = x;
		b = 0;
	}
	else if (1.0 <= hp && hp<= 2.0)
	{
		r = x;
		g = c;
		b = 0;
	}
	else if (2.0 <= hp && hp <= 3.0)
	{
		r = 0;
		g = c;
		b = x;
	}else if (3.0 <= hp&&hp <= 4.0)
	{
		r = 0;
		g = x;
		b = c;
	}
	else if (4.0 <= hp && hp<= 5.0)
	{
		r = x;
		g = 0;
		b = c;
	}
	else if (5.0 <= hp&&hp <= 6.0)
	{
		r = c;
		g = 0;
		b = x;
	}else{//H is undefined

	 r = 0; g = 0; b = 0;
	}
	double m = v - c;//match value
	rgb.R =(unsigned char)( (r + m)*255);
	rgb.G =(unsigned char)((g+ m)*255);
	rgb.B =(unsigned char)((b+ m)*255);
	return rgb;
}

/*
 * save to remote target a RGB image which
 * is the encoding scheme used for computing optical flow images
 *
 *
 * Receives 4 real values and uses them to form an rectangle which is then mapped to an image:
 * Every point in the rectangle P(x,y) is mapped to a color intensity I(j,i).
 * The function then takes numbers from that rectangle, as points P(x,y), computes the HSV color of them
 * based on their x and y coordinates and then transforms that to a RGB color and saves it to the
 * result image as a colored pixel of intensity I at location (j,i), corresponding to location x,y in the rectangle.
 *
 *
 * @params
 * 	xmin,xmax - horizontal minimum and maximum value
 * 	ymin, ymax - vertical maximum value
 *
 */
void showColorMap(double xmin, double xmax, double ymin, double ymax)
{

	int i,j;
	Image *flowmat =NULL;
	flowmat = imaqCreateImage(IMAQ_IMAGE_RGB, 0);
	if(flowmat==NULL)
	{
		fprintf(stderr,"\nerror in function showColorMap\ncould not create image");
		goto errexit;
	}
	if (imaqSetImageSize(flowmat,360, 360)==FALSE)
	{
		fprintf(stderr,"\nerror in function showColorMap\ncould not set image size");
		imaqDispose(flowmat);
		goto errexit;
	}

	for (i = 0; i < 360; i++)
	{
		for (j = 0; j < 360; j++)
		{


			double x = j *((xmax + fabs(xmin)) / 360.0) + xmin;
			double y = i *((ymax + fabs(ymin)) / 360.0) + ymin;

			/*double hue;
			if (y < 0)//c3 || C4
			{
				hue = 180 + 180 + atan2(y, x) * 180.0 / PI;// because atan2 has negative signs when y<0 another 180 degrees are added
			}
			else hue = atan2(y, x) * 180.0 / PI;

			double um = max(fabs(xmax), fabs(xmin));
			double vm = max(fabs(ymin), fabs(ymax));
			double sat = sqrt(y*y + x*x) / sqrt(um*um + vm*vm);

			RGBValue color = HSVToRGBA(hue, sat,3.0/4);*/

			Point po;//(Point*)malloc(sizeof(Point));
			po.x=j;
			po.y=i;
			PixelValue p;
			p.rgb=flowVecToRGB(x,y,xmin,xmax,ymax,ymin,3.0/4);

			if(imaqSetPixel(flowmat,po , p)==FALSE)
			{
				imaqDispose(flowmat);
				goto errexit;
			}
		}
	}
	//imshow("rez image", flowmat);
	if(imaqWriteFile(flowmat, COLOUR_MAP_PATH, NULL)!=FALSE)
	{
		printf("\nColor map saved to file on remote target\n");
	}else{
		imaqDispose(flowmat);
		goto errexit;
	}

	return;
errexit:
	printf("\nerror in colormap");
	if ( (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\nERROR: %s ", tempErrorText);
		imaqDispose(tempErrorText);

	}
}

/*
 * Computes images based on input optical flow results.
 * if defined Macro VERBOUSE is 0 then only an color coded image is
 * computed using to the optical flow vectors u and v from
 * the input
 * if defined Macro VERBOUSE is 1 then apart from the previous image,
 * u and v vectors are saved to txt files: u.txt,v.txt on remote target
 * and also grayscale images are created umat.png,vmat.png and saved
 * @param: optical flow pre cumputed values
 * @return:
 *  - in case of success: an color coded  image of optical flow is returned
 *  - in case of failure: NULL is returned and the last error is
 *  	printed on screen
 */
Image* getFlowImage2(OpticalFlowValues flv,float intensityPercentage)
{
	int i,j;
	//Mat umat = Mat(height, width, CV_8UC1);
	//Mat vmat = Mat(height, width, CV_8UC1);
	Image* flowmat=NULL;
	//uint8_t *lpFlow=(uint8_t*)malloc(height*width*sizeof(uint8_t));// = flowmat.data;

	// Create the Image Buffer
	if((flowmat = imaqCreateImage(IMAQ_IMAGE_RGB, 0))==NULL)
			goto errexit;

	if (imaqSetImageSize(flowmat,flv.width, flv.height)!=TRUE)
	{
		imaqDispose(flowmat);
		goto errexit;
	}

	int w = flv.width;
	if (VERBOUSE == 1)
	{
		Image* umat = NULL;//Mat(flv.height, flv.width, CV_8UC1);
		Image* vmat = NULL;//Mat(flv.height, flv.width, CV_8UC1);
		if((umat = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
			goto errexit;
		if (imaqSetImageSize(umat,flv.width, flv.height)!=TRUE)
			goto errexit;
		if((vmat = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
			goto errexit;
		if (imaqSetImageSize(vmat,flv.width, flv.height)!=TRUE)
			goto errexit;
		FILE *f = fopen("u.txt", "w");
		FILE *g = fopen("v.txt", "w");
		FILE *flow = fopen("flow.txt", "w");
		if (g != NULL && f != NULL&& flow!=NULL)
		{
			printf("\nsaving to files...\n");

			for (i = 0; i < flv.height; i++)
			{
				for (j = 0; j < flv.width; j++)
				{
					fprintf(f, "%f ", flv.u[i*flv.width + j]);
					fprintf(g, "%f ", flv.v[i*flv.width + j]);
					//umat.at<uchar>(i, j) = (uchar)((u[i*w + j] - umin) * 255 / (umax - umin));
					//vmat.at<uchar>(i, j) = (uchar)((v[i*w + j] - vmin) * 255 / (vmax - vmin));
					fprintf(flow, "%c ", getFlowChar2(flv.u[i*w + j], flv.v[i*w + j],flv.umin, flv.umax, flv.vmax, flv.vmin,(float) 0.1));
					//flowmat.at<Vec3b>(i, j) = getFlowVec(u[i*w + j], v[i*w + j],(float) 0.9);

					Point po;//=(Point*)malloc(sizeof(Point));
					po.x=j;
					po.y=i;
					PixelValue p;
					p.rgb=flowVecToRGB(flv.u[i*w + j], flv.v[i*w + j], flv.umin, flv.umax, flv.vmax, flv.vmin, intensityPercentage);
					imaqSetPixel(flowmat,po , p);

					p.grayscale=(uint8_t)((flv.u[i*w + j] - flv.umin) * 255 / (flv.umax - flv.umin));
					imaqSetPixel(umat,po , p);
					p.grayscale=(uint8_t)((flv.v[i*w + j] - flv.vmin) * 255 / (flv.vmax - flv.vmin));
					imaqSetPixel(vmat,po , p);


				}
				fprintf(f, "\n");
				fprintf(g, "\n");
				fprintf(flow, "\n");
			}


			fclose(f);
			fclose(g);
			fclose(flow);
			if(!Log_Vision_Error(imaqWriteFile(vmat, "./vmat.png", NULL)))
			{
				printf("\nvmat saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(umat, "./umat.png", NULL)))
			{
				printf("\numat saved to file on remote target\n");
			}
			//imshow("vmat image", vmat);
			//imshow("umat image", umat);
			//imshow("flowmat image", flowmat);
			printf("\numax=%f", flv.umax);
			printf("\nvmax=%f", flv.vmax);
			printf("\numin=%f", flv.umin);
			printf("\nvmin=%f\n", flv.vmin);
		}
		else{
			perror("could not save data to files");
		}
		imaqDispose(vmat);
		imaqDispose(umat);
	}
	else {//if VERBOUSE==0, compute only the optical flow image

		for (i = 0; i < flv.height; i++)
		{
			for (j = 0; j < flv.width; j++)
			{

				Point po;
				po.x=j;
				po.y=i;
				PixelValue p;
				p.rgb=flowVecToRGB(flv.u[i*w + j], flv.v[i*w + j], flv.umin, flv.umax, flv.vmax, flv.vmin, intensityPercentage);
				imaqSetPixel(flowmat,po , p);
			}
		}
	}

	//if (Log_Vision_Error(imaqArrayToImage(flowmat, lpFlow, width,height)))
	//	goto errexit;

	return flowmat;
errexit:
	printf("\nerror occured in getFlowValue function\n");
	if ( (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\nerror in getFlowImage: %s ", tempErrorText);
		imaqDispose(tempErrorText);
		//imaqDispose(flowmat);

	}
	return NULL;
}


//This function sets the derivative masks and computes the derivatives
int computeDerivatives(Image *src,Image *src2,float *pdfx,float *pdfy,float *pdft,float noiseThresh,int step,int maskSize )
{
	Image *dfx=NULL,*dfy=NULL,*dft=NULL;

	//set derivation masks:
	if(maskSize==2){

		short* pfxmask[] = { fxmask[0], fxmask[1]};
		short* pfymask[] = { fymask[0], fymask[1]};
		short* pftmaskprev[] = { ftmaskprev[0], ftmaskprev[1] };
		short* pftmaskpost[] = { ftmaskpost[0], ftmaskpost[1] };
		//derivate images
		dfx=derivate(src, src2, pfxmask, pfxmask, pdfx, maskSize, noiseThresh,step);
		dfy=derivate(src, src2, pfymask, pfymask, pdfy, maskSize, noiseThresh,step);
		dft=derivate(src, src2, pftmaskprev, pftmaskpost, pdft, maskSize, noiseThresh,step);
	}else if(maskSize==4)
	{

		short* pfxmask[] = { fxprew4[0], fxprew4[1], fxprew4[2], fxprew4[3] };
		short* pfymask[] = { fyprew4[0], fyprew4[1], fyprew4[2], fyprew4[3] };
		short* pftmaskprev[] = { ftprewprev4[0], ftprewprev4[1], ftprewprev4[2], ftprewprev4[3] };
		short* pftmaskpost[] = { ftprewpost4[0], ftprewpost4[1], ftprewpost4[2], ftprewpost4[3] };
		//derivate images
		dfx=derivate(src, src2, pfxmask, pfxmask, pdfx, maskSize, noiseThresh,step);
		dfy=derivate(src, src2, pfymask, pfymask, pdfy, maskSize, noiseThresh,step);
		dft=derivate(src, src2, pftmaskprev, pftmaskpost, pdft, maskSize, noiseThresh,step);
	}
	else//default mask size = 3
	{
		short* pfxmask[] = { fxprew[0], fxprew[1], fxprew[2] };
		short* pfymask[] = { fyprew[0], fyprew[1], fyprew[2] };
		short* pftmaskprev[] = { ftprewprev[0], ftprewprev[1], ftprewprev[2] };
		short* pftmaskpost[] = { ftprewpost[0], ftprewpost[1], ftprewpost[2] };
		//derivate images
		dfx=derivate(src, src2, pfxmask, pfxmask, pdfx, 3, noiseThresh,step);
		dfy=derivate(src, src2, pfymask, pfymask, pdfy, 3, noiseThresh,step);
		dft=derivate(src, src2, pftmaskprev, pftmaskpost, pdft, 3, noiseThresh,step);
	}
	if(dfx==NULL||dfy==NULL||dft==NULL)
	{
		return 1;
	}
	if (VERBOUSE)
	{
		//Display the Captured Image (on x64 Target)
		#if defined(__x86_64__)

			RTDisplayVideoMode displayMode = IMAQ_GRAPHICS_MODE;

			// Set the Display mode to Video
			if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
				goto cleanup;
			//Display the Image
			if(Log_Vision_Error (imaqRTDisplayImage(src, WINDOW_NUMBER, 0)))
				goto cleanup;
			if(Log_Vision_Error (imaqRTDisplayImage(src2, WINDOW_NUMBER, 0)))
							goto cleanup;
			if(Log_Vision_Error (imaqRTDisplayImage(rez, WINDOW_NUMBER, 0)))
							goto cleanup;
			printf("\n Press any key to Continue... \n");
			getchar(); // Wait for user input before the image unload

			// Set the Display mode back to Text
			displayMode = IMAQ_TEXT_MODE;
			if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
					goto cleanup;
		#else
			/*int windowNumber;
			if(Log_Vision_Error(imaqGetWindowHandle(&windowNumber)))
			{
				goto cleanup;
			}else printf("\nfound free window %d\n",windowNumber);
			if(!Log_Vision_Error(imaqDisplayImage(captureImage, windowNumber, TRUE)))
			{
				printf("\nimage displayed in window\n");
			}*/
			// Write the captured image to the file

			if(!Log_Vision_Error(imaqWriteFile(dfx, "./dfx.png", NULL)))
			{
				printf("\ndfx saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(dfy, "./dfy.png", NULL)))
			{
				printf("\ndfy saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(dft, "./dft.png", NULL)))
			{
				printf("\ndft saved to file on remote target\n");
			}


		#endif

	}
	imaqDispose(dfx);
	imaqDispose(dft);
	imaqDispose(dfy);
	return 0;
}
int computeDerivatives2(Image *src,Image *src2,float *pdfx,float *pdfy,float *pdft,float noiseThresh,int step,int maskSize )
{
	Image *dfx=NULL,*dfy=NULL,*dft=NULL;

	//set derivation masks:
	short** pfxmask = NULL;
	short** pfymask = NULL;
	short** pftmaskprev = NULL;
	short** pftmaskpost= NULL;
	pfxmask = setMask(pfxmask, maskSize, FXMASK);
	pfymask = setMask(pfymask, maskSize, FYMASK);
	pftmaskpost = setMask(pftmaskpost, maskSize, FTMASKPOST);
	pftmaskprev = setMask(pftmaskprev, maskSize, FTMASKPREV);

	//derivate images
	dfx=derivate(src, src2, pfxmask, pfxmask, pdfx, maskSize, noiseThresh,step);
	dfy=derivate(src, src2, pfymask, pfymask, pdfy, maskSize, noiseThresh,step);
	dft=derivate(src, src2, pftmaskprev, pftmaskpost, pdft, maskSize, noiseThresh,step);

	//in case derivatives cannot be computed, return error code 1
	if(dfx==NULL||dfy==NULL||dft==NULL)
	{
		return 1;
	}
	if (VERBOUSE)
	{
		//Display the Captured Image (on x64 Target)
		#if defined(__x86_64__)

			RTDisplayVideoMode displayMode = IMAQ_GRAPHICS_MODE;

			// Set the Display mode to Video
			if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
				goto cleanup;
			//Display the Image
			if(Log_Vision_Error (imaqRTDisplayImage(src, WINDOW_NUMBER, 0)))
				goto cleanup;
			if(Log_Vision_Error (imaqRTDisplayImage(src2, WINDOW_NUMBER, 0)))
							goto cleanup;
			if(Log_Vision_Error (imaqRTDisplayImage(rez, WINDOW_NUMBER, 0)))
							goto cleanup;
			printf("\n Press any key to Continue... \n");
			getchar(); // Wait for user input before the image unload

			// Set the Display mode back to Text
			displayMode = IMAQ_TEXT_MODE;
			if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
					goto cleanup;
		#else
			/*int windowNumber;
			if(Log_Vision_Error(imaqGetWindowHandle(&windowNumber)))
			{
				goto cleanup;
			}else printf("\nfound free window %d\n",windowNumber);
			if(!Log_Vision_Error(imaqDisplayImage(captureImage, windowNumber, TRUE)))
			{
				printf("\nimage displayed in window\n");
			}*/
			// Write the captured image to the file

			if(!Log_Vision_Error(imaqWriteFile(dfx, "./dfx.png", NULL)))
			{
				printf("\ndfx saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(dfy, "./dfy.png", NULL)))
			{
				printf("\ndfy saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(dft, "./dft.png", NULL)))
			{
				printf("\ndft saved to file on remote target\n");
			}


		#endif

	}
	imaqDispose(dfx);
	imaqDispose(dft);
	imaqDispose(dfy);
	return 0;
}

//set an derivative mask according to specified type and size
short **setMask(short** pfx,int size, maskType mask)
{
	pfx = (short**)malloc(size*sizeof(short*));
	switch (mask)
	{
	case FXMASK:
		if (size == 2)
		{
			pfx[0] = fxmask[0];
			pfx[1] = fxmask[1];
			return pfx;
		}
		if (size == 3)
		{
			pfx[0] = fxprew[0];
			pfx[1] = fxprew[1];
			pfx[2] = fxprew[2];

			return pfx;
		}
		if (size == 4)
		{
			pfx[0] = fxprew4[0];
			pfx[1] = fxprew4[1];
			pfx[2] = fxprew4[2];
			pfx[3] = fxprew4[3];
			return pfx;
		}

		break;
	case FYMASK:
		if (size == 2)
		{
			pfx[0] = fymask[0];
			pfx[1] = fymask[1];
			return pfx;
		}
		if (size == 3)
		{
			pfx[0] = fyprew[0];
			pfx[1] = fyprew[1];
			pfx[2] = fyprew[2];
			return pfx;
		}
		if (size == 4)
		{
			pfx[0] = fyprew4[0];
			pfx[1] = fyprew4[1];
			pfx[2] = fyprew4[2];
			pfx[3] = fyprew4[3];
			return pfx;
		}
		break;
	case FTMASKPOST:
		if (size == 2)
		{
			pfx[0] = ftmaskpost[0];
			pfx[1] = ftmaskpost[1];
			return pfx;
		}
		if (size == 3)
		{
			pfx[0] = ftprewpost[0];
			pfx[1] = ftprewpost[1];
			pfx[2] = ftprewpost[2];
			return pfx;
		}
		if (size == 4)
		{
			pfx[0] = ftprewpost4[0];
			pfx[1] = ftprewpost4[1];
			pfx[2] = ftprewpost4[2];
			pfx[3] = ftprewpost4[3];
			return pfx;
		}
		break;
	case FTMASKPREV:
		if (size == 2)
		{
			pfx[0] = ftmaskprev[0];
			pfx[1] = ftmaskprev[1];
			return pfx;
		}
		if (size == 3)
		{
			pfx[0] = ftprewprev[0];
			pfx[1] = ftprewprev[1];
			pfx[2] = ftprewprev[2];
			return pfx;
		}
		if (size == 4)
		{
			pfx[0] = ftprewprev4[0];
			pfx[1] = ftprewprev4[1];
			pfx[2] = ftprewprev4[2];
			pfx[3] = ftprewprev4[3];
			return pfx;
		}
		break;
	default :
		return NULL;
	}


	return NULL;
}
