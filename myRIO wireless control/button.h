/*
 * button.h
 *
 *  Created on: 10.06.2018
 *      Author: Adi
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include "MyRio.h"
#if NiFpga_Cpp
extern "C" {
#endif

NiFpga_Bool buttonValue();

#if NiFpga_Cpp
}
#endif


#endif /* BUTTON_H_ */
