/*
 * opticalFlow.h
 *
 *  Created on: 09.04.2017
 *      Author: Adi
 */

#ifndef OPTICALFLOW_H_
#define OPTICALFLOW_H_
// Vision  Includes
#include <nivision.h>
#include <NIIMAQdx.h>
#include <stdbool.h>
/*
 * 1. Import the project onto Eclipse IDE.
2. Connect the Camera and update the Camera name("CAM_NAME" in opticalFlow.h).
3. Build the Project.
	"Project Explorer->Image Acquisition->Build Configurations->Build all"
4. Copy the Executable onto the Target,
	a. 'armv7\ImageAcquisition' for ARM
    b. 'x64\ImageAcquisition' for x64 Target.
5. Output Image (Captured Image),
	a. x64 based target - Will be displayed on the Screen .
	b. armv7 based target - Write the Captured Image to "./capturedImage.png"

Note:to see the captured image, open NI MAX, expand Remote Systems and right
click on myRIO�s name and select File Transfer. Enter �admin� and click Ok.
Go to home/admin/test and click on �capturedImage2.png� to see the image.
 */
//local definitions
#define CAM_NAME    			"cam3"
#define CAPTURED_IMAGE1_PATH    "./capturedImage1.png"
#define CAPTURED_IMAGE2_PATH    "./capturedImage2.png"
#define REZ_IMAGE_PATH     		"./RezImage.png"
#define COLOUR_MAP_PATH 		"./cmap.png"


extern bool VERBOUSE;
extern bool opticalFlowFinished;
//Laplacian filter
extern const float lap[3][3] ;
// Local Definitions of Derivative kernels
//Roberts 2x2 kernels
extern short fxmask[2][2];//convolve with both images
extern short fymask[2][2];//convolve with both images
extern short ftmaskprev[2][2];//convolve with first image
extern short ftmaskpost[2][2];//convolve with second image
//Prewitt 3x3 kernels
extern short fxprew[3][3];//convolve with both images
extern short fyprew[3][3];//convolve with both images
extern short ftprewprev[3][3];//convolve with first image
extern short ftprewpost[3][3];//convolve with second image
//Prewitt 4x4 kernels
extern short fxprew4[4][4];//convolve with both images
extern short fyprew4[4][4];//convolve with both images
extern short ftprewprev4[4][4];//convolve with first image
extern short ftprewpost4[4][4];//convolve with second image
/*
* Optical flow values:
 * 		- u 	= x component of image velocity or optical flow for every pixel in the image
 * 		- v		= y component of image velocity or optical flow for every pixel in the image
 * 		- umax 	= maximum value of u vector
 * 		- umin	= minimum value of u vector
 * 		- vmin 	= minimum value of v vector
 * 		- vmax 	= maximum value of v vector
 * 		- height, width = number of rows and number or columns of u and v regarded as matrices
 */
typedef struct OpticalFlow_struct{
	float *u;	//dx/dt
	float *v;	//dy/dt
	int height;
	int width;
	double umin;
	double umax;
	double vmin;
	double vmax;
}OpticalFlowValues;

//structure for storing parameters of function opticalFlow
struct OF_data {
    Image* src;
    Image* src2;
    float lambda;
    float intensityPercentage;
    float iterationThresh;
    float noiseThreshnold;
    Image* rez;
    int step;
    int maskSize;
    int levels;
};

typedef enum { FXMASK, FYMASK, FTMASKPOST, FTMASKPREV } maskType;
typedef enum { LucasKanade, HornSchunck, LucasKanadePyramids} opticalFlowMethod;
#if NiFpga_Cpp
extern "C" {
#endif
char getFlowChar2(float x,float y,float umin, float umax,float vmax,float vmin,float thresh);
RGBValue HSVToRGBA(double h,double s,double v);
void showColorMap(double xmin, double xmax, double ymin, double ymax);
bool Log_Vision_Error(int errorValue);
bool Log_Imaqdx_Error(IMAQdxError errorValue);
void* captureImage(void* src);
Image* derivate(Image *img1, Image *img2, short *mask1[], short *mask2[], float *pRez,int maskSize ,float thresh,int step);
Image* displacementDerivate(Image* img1, Image* img2, short *mask1[], short *mask2[], float *pRez,int maskSize ,float thresh,int step,float *disU,float *disV);
float applyLaplace(float *lpSrc, int w, int i, int j,const float mask[3][3]);
RGBValue flowVecToRGB(float x, float y, float umin, float umax,float vmax,float vmin,float luminosity);

int computeDerivatives(Image *src,Image *src2,float *pdfx,float *pdfy,float *pdft,float noiseThresh,int step,int maskSize );
int computeDerivatives2(Image *src,Image *src2,float *pdfx,float *pdfy,float *pdft,float noiseThresh,int step,int maskSize );
Image* getFlowImage2(OpticalFlowValues flv,float intensityPercentage);
void setDerivationMasks(short *pfxmask[],short* pfymask[],short* pftmaskprev[],short* pftmaskpost[],int maskSize);
short **setMask(short** pfx,int size, maskType mask);
#if NiFpga_Cpp
}
#endif

#endif /* OPTICALFLOW_H_ */
