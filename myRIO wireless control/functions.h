/*
 * functions.h
 *
 *  Created on: 11.04.2017
 *      Author: Adi
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#define PI 3.14159265
#if NiFpga_Cpp
extern "C" {
#endif

void sleepFor(int microseconds);
unsigned long getMicroseconds();
void waitFor(int microseconds);
int min(int a,int b);
int max(int a,int b);
#if NiFpga_Cpp
}
#endif


#endif /* FUNCTIONS_H_ */
