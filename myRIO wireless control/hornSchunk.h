/*
 * hornSchunk.h
 *
 *  Created on: 23.04.2017
 *      Author: Adi
 */

#ifndef HORNSCHUNK_H_
#define HORNSCHUNK_H_

#include "opticalFlow.h"

#if NiFpga_Cpp
extern "C" {
#endif
void* opticalFlowThreadable(void *dataArg);
OpticalFlowValues hornSchunk(int height,int width,float* dfx,float* dfy,float* dft,float lambda,double iterationThresh);
Image* opticalFlow(Image *src, Image *src2,float lambda,float intensityPercentage,float iterationThresh,float noiseThresh,int step,int maskSize);
#if NiFpga_Cpp
}
#endif

#endif /* HORNSCHUNK_H_ */
