/*
 * SensorMotor.h
 *
 *  Created on: Sep 4, 2015
 *      Author: So
 */

#ifndef SENSORMOTOR_H_
#define SENSORMOTOR_H_

#include "MyRio.h"

#if NiFpga_Cpp
extern "C" {
#endif


int position(int p);


#if NiFpga_Cpp
}
#endif


#endif /* SENSORMOTOR_H_ */
