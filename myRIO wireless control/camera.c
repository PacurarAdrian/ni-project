/*
 * camera.c
 *
 *  Created on: 02.03.2017
 *      Author: Adi
 */

#include "camera.h"
#include <locale.h>
#include <wchar.h>

// Local Definitions

#define CAM_NAME    "cam3"
#define CAPTURED_IMAGE_PATH     "./capturedImage.png"

#define WINDOW_NUMBER    15    // Image Display Window Number
#define IMAQDX_ERROR_MESSAGE_LENGTH    256


/*
 * 1. Import the project onto Eclipse IDE.
2. Connect the Camera and update the Camera name("CAM_NAME" in ImageAcquisition.c).
3. Build the Project.
	"Project Explorer->Image Acquisition->Build Configurations->Build all"
4. Copy the Executable onto the Target,
	a. 'armv7\ImageAcquisition' for ARM
    b. 'x64\ImageAcquisition' for x64 Target.
5. Output Image (Captured Image),
	a. x64 based target - Will be displayed on the Screen .
	b. armv7 based target - Write the Captured Image to "./capturedImage.png"

Note:to see the captured image, open NI MAX, expand Remote Systems and right
click on myRIO�s name and select File Transfer. Enter �admin� and click Ok.
Go to home/admin/test and click on �capturedImage2.png� to see the image.
 */
int capture(void) {


	printf("\n ++ ImageAcquisitionAndDisplayExample \n");

	Image* captureImage = NULL;
	IMAQdxSession session = 0;

	// Create the Image Buffer
	captureImage = imaqCreateImage(IMAQ_IMAGE_U8, 0);

	// Open a session to the selected camera
	if (Log_Imaqdx_Error(IMAQdxOpenCamera(CAM_NAME, IMAQdxCameraControlModeController, &session)))
		goto cleanup;

	// Acquire an image
	if (Log_Imaqdx_Error(IMAQdxSnap(session, captureImage)))
		goto cleanup;

	// Process the Captured image here.  For now we are simply displaying or logging
	// the image to file.

	//Display the Captured Image (on x64 Target)
#if defined(__x86_64__)

	RTDisplayVideoMode displayMode = IMAQ_GRAPHICS_MODE;

	// Set the Display mode to Video
	if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
		goto cleanup;
	//Display the Image
	if(Log_Vision_Error (imaqRTDisplayImage(captureImage, WINDOW_NUMBER, 0)))
		goto cleanup;

	printf("\n Press any key to Continue... \n");
	getchar(); // Wait for user input before the image unload

	// Set the Display mode back to Text
	displayMode = IMAQ_TEXT_MODE;
	if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
			goto cleanup;
#else
	/*int windowNumber;
	if(Log_Vision_Error(imaqGetWindowHandle(&windowNumber)))
	{
		goto cleanup;
	}else printf("\nfound free window %d\n",windowNumber);
	if(!Log_Vision_Error(imaqDisplayImage(captureImage, windowNumber, TRUE)))
	{
		printf("\nimage displayed in window\n");
	}*/
	// Write the captured image to the file
	if(!Log_Vision_Error(imaqWriteFile(captureImage, CAPTURED_IMAGE_PATH, NULL)))
	{
		printf("\nimage saved to file on remote target\n");
	}

#endif

cleanup:
	//Close the Camera session
	IMAQdxCloseCamera (session);

	// Dispose the image
	imaqDispose(captureImage);

	printf("\n -- ImageAcquisitionAndDisplayExample \n");
	return 0;

}


