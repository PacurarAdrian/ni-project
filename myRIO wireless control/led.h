/*
 * Led.h
 *
 *  Created on: 10.06.2018
 *      Author: Adi
 */

#ifndef LED_H_
#define LED_H_

#include "MyRio.h"
#if NiFpga_Cpp
extern "C" {
#endif

void switchLed(uint8_t bit, NiFpga_Bool do_A0);

#if NiFpga_Cpp
}
#endif


#endif /* LED_H_ */
