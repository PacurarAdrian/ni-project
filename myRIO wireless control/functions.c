/*
 * functions.c
 *
 *  Created on: 11.04.2017
 *      Author: Adi
 */

#include "functions.h"
//#include "MyRio.h"
//#include "math.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
int min(int a,int b)
{
	if(a<=b)
		return a;
	else return b;
}
int max(int a,int b)
{
	if(a>b)
		return a;
	else return b;
}
void sleepFor(int microseconds)
{
	struct timespec ts;
	ts.tv_sec = microseconds / 1000000;
	ts.tv_nsec = (microseconds % 1000000) * 1000;
	nanosleep(&ts, NULL);
}
unsigned long getMicroseconds()
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	unsigned long time_in_micros = 1000000 * tv.tv_sec + tv.tv_usec;
	return time_in_micros;
}

void waitFor(int microseconds)
{
	unsigned long currentTime;
	unsigned long finalTime;
	currentTime=getMicroseconds();
	finalTime = currentTime + microseconds;
	while (currentTime < finalTime)
	{
		currentTime=getMicroseconds();
	};
}
