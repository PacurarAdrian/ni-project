/*
 * Forward.h
 *
 *  Created on: Sep 1, 2017
 *      Author: Adi
 */

#ifndef FORWARD_H_
#define FORWARD_H_

#include "MyRio.h"
#include "PWM.h"

#if NiFpga_Cpp
extern "C" {
#endif


int forward();
void initializePwm(MyRio_Pwm *pwmA2,MyRio_Pwm *pwmA1);
int setSpeedForward(MyRio_Pwm *pwmA2,MyRio_Pwm *pwmA1,int gear);
int setSpeedRight(MyRio_Pwm *pwmA2,int gear,int speed);
int setSpeedLeft(MyRio_Pwm *pwmA1,int gear,int speed);
NiFpga_Status goForward2(MyRio_Pwm pwmA2,MyRio_Pwm pwmA1,int gear,int stepsLeft,int stepsRight,int speedRight,int speedLeft);
NiFpga_Status goBackward2(MyRio_Pwm pwmA2,MyRio_Pwm pwmA1,int gear,int stepsLeft,int stepsRight,int speedRight,int speedLeft);
#if NiFpga_Cpp
}
#endif


#endif /* FORWARD_H_ */
