/*
 *  Created on: 10.06.2018
 *      Author: Adi
 */

#include <stdio.h>
#include "DI.h"
#include "MyRio.h"
#include "button.h"

/**
 * Overview:
 * Demonstrates using the digital input for buttons (DI.BTN). Reads initial values
 * of digital input channel 0 . Prints the values to the console.
 *
 * Output:
 * The program reads the initial voltage on DI.BTN 0  on. Input voltage is written to the
 * console and is returned.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */
NiFpga_Bool buttonValue()
{

    NiFpga_Bool di_A0;


    MyRio_Di A0;

   // printf("read button Input\n");

    /*
     * Specify the registers that correspond to the DI channel
     * that needs to be accessed.
     */
    A0.in = DIBTN;
    A0.bit = 0;

    /*
     * Read from DI channel DI.BTN0.
     */
    di_A0 = Di_ReadBit(&A0);


    /*
     * Print out the logic level of the channel.
     */
   // printf("DI.BTN0 = %d\n", di_A0);

    return di_A0;
}
