/*
 * lucasKanade.h
 *
 *  Created on: 23.04.2017
 *      Author: Adi
 */

#ifndef LUCASKANADE_H_
#define LUCASKANADE_H_

#include "opticalFlow.h"

#if NiFpga_Cpp
extern "C" {
#endif
OpticalFlowValues lucasKanade(int height, int width, float* dfx, float* dfy, float*dft);
void* LKopticalFlowThreadable(void *dataArg);
Image* LKopticalFlow(Image *src, Image *src2,float intensityPercentage,float noiseThresh,int step,int maskSize);
#if NiFpga_Cpp
}
#endif

#endif /* LUCASKANADE_H_ */
