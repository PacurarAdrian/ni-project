/*
 * Example for performing Basic Digital Input
 *
  *  Created on: 10.06.2018
 *      Author: Adi
 */

#include <stdio.h>

/*
 * Include the myRIO header file.
 * The target type must be defined in your project, as a stand-alone #define,
 * or when calling the compiler from the command-line.
 */
#include "MyRio.h"
#include "DI.h"


/*
 * Declare the myRIO NiFpga_Session so that it can be used by any function in
 * this file. The variable is actually defined in myRIO.c.
 *
 * This removes the need to pass the myrio_session around to every function and
 * only has to be declared when it is being used.
 */
extern NiFpga_Session myrio_session;



/**
 * Read the value of a single channel in the 8 channel bank.
 *
 * The DI.BTN channels are accessed in 8-bit banks where each bit in the bank
 * corresponds to one of the DI channels. In general:
 *    DI.BTN0 = bit0   (1st channel on the DI 7:0 bank)
 *
 * A DI channel can be only input
 *
 * The voltage level present at the DIO channel pin can be determined based on
 * the value of the IN register. A value of 0 means a low voltage (0 volts) is
 * present, a value of 1 means a high voltage (3.3 volts) is present.
 *
 * @param[in]  channel  A struct containing the registers and bit location for
 *                      the DI channel to be read from
 * @return NiFpga_Bool which is the logical value of the voltage on the channel
 */
NiFpga_Bool Di_ReadBit(MyRio_Di* channel)
{
    NiFpga_Status status;
    uint8_t inValue;
    /*
     * Get the value of the input register.
     *
     * NiFpga_MergeStatus is used to propagate any errors from previous
     * function calls. Errors are not anticipated so error checking is not done
     * after every NiFpga function call but only at specific points.
     */
   status= NiFpga_ReadU8(myrio_session, channel->in, &inValue);

    /*
     * Check if there was an error writing to or reading from the DIO channel
     * registers.
     *
     * If there was an error then the rest of the function cannot complete
     * correctly so print an error message to stdout and return from the
     * function early.
     */
    MyRio_ReturnValueIfNotSuccess(status, NiFpga_False,
        "Could not read from the DI.BTN channel in register!")

    /*
     * Isolate the value of the relevant bit.
     */
    inValue = inValue & (1 << channel->bit);

    /*
     *  If inValue > 0 then return NiFpga_True otherwise return NiFpga_False.
     */
    return (inValue > 0) ? NiFpga_True : NiFpga_False;
}
