/*
 * lucasKanade.c
 *
 *  Created on: 23.04.2017
 *      Author: Adi
 */
#include "lucasKanadePyramids.h"

#include "float.h"
#include "math.h"
#include <stdlib.h>
#include <stdio.h>
#include "MyRio.h"
#include "functions.h"
#include <pthread.h>





Image* reduce(Image* img,double w[5][5])
{
	int height;// = img.rows/2;
	int width;// = img.cols/2;

	//if (imaqGetImageSize(img, &width, &height)!=TRUE)
	//			goto errexit;
	uint8_t *lpImg = imaqImageToArray(img, IMAQ_NO_RECT, &width, &height);
	int wi=width;
	if(lpImg==NULL)
	{
		goto errexit;
	}
	height=height/2;
	width=width/2;

	Image* rez=NULL;// = Mat(height, width, CV_8UC1);
	if((rez = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
		goto errexit;
	if (imaqSetImageSize(rez,width, height)==FALSE)
	{
		fprintf(stderr,"\nerror in function reduce\ncould not set image size");
		imaqDispose(rez);
		goto errexit;
	}
	int i;
	int j;

	for (i = 1; i < height-1; i++)
	{
		for (j = 1; j < width-1; j ++)
		{
			int sum = 0;
			int m,n;
			for (m = -2; m <= 2; m++)
				for (n = -2; n <= 2;n++)
					//sum += img.at<uchar>(2*i+m, 2*j+n)*w[m][n];
				sum += lpImg[(2*i+m)*wi + 2*j+n]*w[m][n];
			//rez.at<uchar>(i, j) = sum;
			Point po;//(Point*)malloc(sizeof(Point));
			po.x=j;
			po.y=i;
			PixelValue p;
			p.grayscale=(uint8_t)(sum);
			imaqSetPixel(rez,po , p);
		}
	}

	return rez;
	errexit:
		printf("\nerror occured in function reduce\n");
		if ( (imaqGetLastError() != ERR_SUCCESS)) {
			char *tempErrorText = imaqGetErrorText(imaqGetLastError());
			printf("\n %s ", tempErrorText);
			imaqDispose(tempErrorText);

		}
	return NULL;
}

double bilinearInterpolate(int xo,int yo,int xu,int yu,double y,double x,float *u,int width)
{
	return (xo - x)*(yo - y)*u[yo*width + xo] + (x - xu)*(yo - y)*u[yo*width + xu] + (xo - x)*(y - yu)*u[yu*width + xo] + (x - xu)*(y - yu)*u[yu*width + xu];
}
double linearInterpolatex(int xo, int xu, int y, double x, float *u, int width)
{
	return (x - xu)*u[y*width + xo] + (xo - x)*u[y*width + xu];
}
double linearInterpolatey(int yo, int yu, double y,int x, float *u, int width)
{
	return (y - yu)*u[yo*width + x] + (yo - y)*u[yu*width + x];
}
/*
 * This function is used for calling function
 * LKopticalFlow when that function must be
 * executed in a separate thread created
 * with pthread_create
 * @params:
 * 	-dataArg - contains the input parameters of function
 * 			LKopticalFlow and also the return value of that function
 *
 */
void* LKPyramidsThreadable(void *dataArg)
{
	struct OF_data *data;
	data=(struct OF_data *)dataArg;
	Image *src;//=data->src;
	Image *src2;//=data->src2;
	float noiseThresh=data->noiseThreshnold;
	if(data->step!=1)
	{
		if((src = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
				goto errexit;
		if((src2 = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
				goto errexit;
		if(imaqScale(src,data->src,data->step,data->step,IMAQ_SCALE_SMALLER,IMAQ_NO_RECT)!=TRUE)
			goto errexit;
		else{
			printf("\nimage1 scaled to half it's size");
		}
		if(imaqScale(src2,data->src2,data->step,data->step,IMAQ_SCALE_SMALLER,IMAQ_NO_RECT)!=TRUE)
			goto errexit;
		else{
			printf("\nimage2 scaled to half it's size");
		}
	}else{
		src=data->src;
		src2=data->src2;
	}
	printf("pana aci mere\n");
	Image* rez=pyramids(src,src2,noiseThresh,data->intensityPercentage,data->maskSize,data->levels);
	data->rez=rez;
	opticalFlowFinished=true;
	pthread_exit(NULL);
errexit:
	printf("\nerror occured in function LK Pyramids threadable\n");
		if ( (imaqGetLastError() != ERR_SUCCESS)) {
			char *tempErrorText = imaqGetErrorText(imaqGetLastError());
			printf("\n %s ", tempErrorText);
			imaqDispose(tempErrorText);

		}
	pthread_exit(NULL);
}


Image* pyramids(Image *src, Image *src2, float noiseThresh,float intensityPercentage,int maskSize,int levels)//call with noise=0, step=1
{
	int step=1;
	int i,j,level;
	double a = 0.4;
	double w[5] = { 1.0 / 4 - a / 2, 1.0 / 4, a, 1.0 / 4, 1.0 / 4 - a / 2 };
	double wp[5][5];
	for ( i = 0; i < 5; i++)
		for ( j = 0; j < 5; j++)
			wp[i][j] = w[i] * w[j];
	short** pfxmask=NULL;
	short** pfymask=NULL;
	short** pftmaskprev=NULL;
	short** pftmaskpost=NULL;
	pfxmask = setMask(pfxmask, maskSize, FXMASK);
	pfymask = setMask(pfymask, maskSize, FYMASK);
	pftmaskpost = setMask(pftmaskpost, maskSize, FTMASKPOST);
	pftmaskprev = setMask(pftmaskprev, maskSize, FTMASKPREV);
	/*printf("mask x:\n");
	for (i = 0; i < maskSize; i++)
	{
		for (j = 0; j < maskSize; j++)
			printf("%d ", pfxmask[i][j]);
		printf("\n");
	}*/

	OpticalFlowValues flv,flv2;
	Image **redSrc=(Image**)malloc(2*levels*sizeof(Image*));


	//initialize level 0
	redSrc[0*2+0] = src;
	redSrc[0*2+1] = src2;

	//compute reduced images for levels of the pyramid
	for (level = 1; level < levels; level++)
	{
		redSrc[level*2+0] = reduce(redSrc[(level - 1)*2+0], wp);
		redSrc[level*2+1] = reduce(redSrc[(level - 1)*2+1], wp);
	}


	for (level = levels-1; level >=0; level--)
	{
		int height;// = redSrc.rows;
		int width;// = redSrc.cols;
		if (imaqGetImageSize(redSrc[2*level+0], &width, &height)!=TRUE)
			goto errexit;

		Image *dfx,*dfy, *dft;

		float *pdfx = (float*)malloc(height*width*sizeof(float));
		float *pdfy = (float*)malloc(height*width*sizeof(float));
		float *pdft = (float*)malloc(height*width*sizeof(float));

		//compute optical flow for 1st level
		dfx = derivate(redSrc[2*level+0], redSrc[2*level+1], pfxmask, pfxmask, pdfx, maskSize, noiseThresh, step);
		dfy = derivate(redSrc[2*level+0], redSrc[2*level+1], pfymask, pfymask, pdfy, maskSize, noiseThresh, step);
		if (level == levels - 1)
			dft = derivate(redSrc[2*level+0], redSrc[2*level+1], pftmaskprev, pftmaskpost, pdft, maskSize, noiseThresh, step);
		else
			dft = displacementDerivate(redSrc[0+2*level], redSrc[1+2*level], pftmaskprev, pftmaskpost, pdft, maskSize, noiseThresh, step, flv2.u, flv2.v);
		if(dfx==NULL||dfy==NULL||dft==NULL)
		{
			goto errexit;
		}
		flv = lucasKanade(height, width, pdfx, pdfy, pdft);
		/*Mat rezRed = getFlowImage(flv);
		if (level == 1)
		imshow("rez red1:", dft);
		if (level == 2)
		imshow("rez red2:", dft);

		if (level == 0)
		imshow("rez red0:", dft);
		*/
		if (level != levels-1)
		{
			//add optical flows
			for (i = 0; i < height; i++)
				for (j = 0; j < width; j++)
				{
					flv2.u[i*width + j] += flv.u[i*width + j];
					flv2.v[i*width + j] += flv.v[i*width + j];
					if (flv2.u[i*width + j] > flv2.umax)
					{
						flv2.umax = flv2.u[i*width + j];
					}
					else if (flv2.u[i*width + j] < flv2.umin)
					{
						flv2.umin = flv2.u[i*width + j];
					}
					if (flv2.v[i*width + j] > flv2.vmax)
					{
						flv2.vmax = flv2.v[i*width + j];
					}
					else if (flv2.v[i*width + j] < flv2.vmin)
					{
						flv2.vmin = flv2.v[i*width + j];
					}
				}
			free(flv.u);
			free(flv.v);
		}else {
			flv2.u = flv.u;
			flv2.v = flv.v;
			flv2.height = flv.height;
			flv2.width = flv.width;
			flv2.umax = flv.umax;
			flv2.umin = flv.umin;
			flv2.vmax = flv.vmax;
			flv2.vmin = flv.vmin;
		}
		if (level != 0)
		{
			//interpolate to obtain matrices of twice the size and multiply flow by 2
			height *= 2;
			width *= 2;
			float *u = (float*)malloc(height*width*sizeof(float));
			float *v = (float*)malloc(height*width*sizeof(float));

			for (i = 0; i < height; i++)
				for (j = 0; j < width; j++)
				{
					// just multiply by two the points which correspond directly to the ones in the 1st level
					if (ceil(i / 2.0) == i / 2 && ceil(j / 2.0) == j / 2)
					{
						u[i*width + j] = 2*flv2.u[i / 2 * width / 2 + j / 2];
						v[i*width + j] = 2*flv2.v[i / 2 * width / 2 + j / 2];
					}
					else
					{  //compute points coordinates corresponding to first level
						int xu = j / 2;
						int yu = i / 2;
						int xo = xu + 1;
						int yo = yu + 1;
						// in case the bottom left corner point does not have a correspondent
						// it takes the values of corner point in previous level multiplied by 2
						if (yo >= height / 2 && xo >= width / 2)
						{
							u[i*width + j] = 2*flv2.u[i / 2 * width / 2 + j / 2];
							v[i*width + j] = 2*flv2.v[i / 2 * width / 2 + j / 2];
						}
						else
							//compute values for points belonging to the bottom margin
							if (yo >= height / 2)
							{
								u[i*width + j] =2* linearInterpolatex(xo, xu, i / 2, j / 2.0, flv2.u, width / 2);
								v[i*width + j] = 2*linearInterpolatex(xo, xu, i / 2, j / 2.0, flv2.v, width / 2);
							}
							else
								//compute values for points belonging to the left margin
								if (xo >= width / 2)
								{
									u[i*width + j] = 2*linearInterpolatey(yo, yu, i / 2.0, j / 2, flv2.u, width / 2);
									v[i*width + j] = 2*linearInterpolatey(yo, yu, i / 2.0, j / 2, flv2.v, width / 2);
								}
								else//compute values for the rest (points in the middle)
								{
									u[i*width + j] = 2*bilinearInterpolate(xo, yo, xu, yu, i / 2.0, j / 2.0, flv2.u, width / 2);
									v[i*width + j] = 2*bilinearInterpolate(xo, yo, xu, yu, i / 2.0, j / 2.0, flv2.v, width / 2);
								}

					}
				}
			free(flv2.u);
			free(flv2.v);
			flv2.u = u;
			flv2.v = v;
			//multiply flow by 2
			flv2.umax *= 2 ;
			flv2.umin *= 2;
			flv2.vmax *= 2 ;
			flv2.vmin *= 2;

			flv2.height = height;
			flv2.width = width;
			//rez = getFlowImage(flv);
			//imshow("rez interpolation:", rez);
		}

		if (VERBOUSE == 1)
		{
			if(!Log_Vision_Error(imaqWriteFile(redSrc[0+2*level], "./rezReduce.png", NULL)))
			{
				printf("\nReduced source 1 saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(redSrc[1+2*level], "./rezReduce2.png", NULL)))
			{
				printf("\nReduced source 2 saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(dfx, "./dfx.png", NULL)))
			{
				printf("\ndx saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(dfy, "./dfy.png", NULL)))
			{
				printf("\ndy saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(dft, "./dft.png", NULL)))
			{
				printf("\ndt saved to file on remote target\n");
			}
		}


		//free(flv.u);
		//free(flv.v);
		free(pdfx);
		free(pdfy);
		free(pdft);

		imaqDispose(dfx);
		imaqDispose(dft);
		imaqDispose(dfy);
		if(level!=0)
		{
			imaqDispose(redSrc[0+2*level]);
			imaqDispose(redSrc[1+2*level]);
		}
	}
	Image* rez = getFlowImage2(flv2,intensityPercentage);
	free(flv2.u);
	free(flv2.v);
	free(redSrc);
	return rez;

	errexit:
		printf("\nerror occured in function pyramids2 \n");
		if ( (imaqGetLastError() != ERR_SUCCESS)) {
			char *tempErrorText = imaqGetErrorText(imaqGetLastError());
			printf("\n %s ", tempErrorText);
			imaqDispose(tempErrorText);

		}
		return NULL;
}
