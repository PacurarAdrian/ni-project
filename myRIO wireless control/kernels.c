/*
 * kernels.c
 *
 *  Created on: 27.11.2017
 *      Author: Adi
 */
#include "opticalFlow.h"

bool VERBOUSE=false;
bool opticalFlowFinished=false;
//Laplacian filter
const float lap[3][3] = { { 1/12.0, 1 / 6.0, 1/12.0 }, { 1 / 6.0, -1, 1 / 6.0 }, { 1/12.0, 1 / 6.0, 1/12.0 } };
// Local Definitions of Derivative kernels
//Roberts 2x2 kernels
short fxmask[2][2] = { { -1, 1 }, { -1, 1 } };//convolve with both images
short fymask[2][2] = { { -1, -1 }, { 1, 1 } };//convolve with both images
short ftmaskprev[2][2] = { { -1, -1 }, { -1, -1 } };//convolve with first image
short ftmaskpost[2][2] = { { 1, 1 }, { 1, 1 } };//convolve with second image
//Prewitt 3x3 kernels
short fxprew[3][3] = { { -1, 0, 1 }, { -1, 0, 1 }, { -1, 0, 1 } };//convolve with both images
short fyprew[3][3] = { { -1, -1, -1 }, { 0, 0, 0 }, { 1, 1, 1 } };//convolve with both images
short ftprewprev[3][3] = { { -1, -1, -1 }, { -1, -1, -1 }, { -1, -1, -1 } };//convolve with first image
short ftprewpost[3][3] = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };//convolve with second image
//Prewitt 4x4 kernels
short fxprew4[4][4] = { { -3, -1, 1, 3 }, { -3, -1, 1, 3 }, { -3, -1, 1, 3 }, { -3, -1, 1, 3 } };//convolve with both images
short fyprew4[4][4] = { { -3, -3, -3, -3 }, { -1, -1, -1, -1 }, { 1, 1, 1, 1 }, { 3, 3, 3, 3 } };//convolve with both images
short ftprewprev4[4][4] = { { -1, -1, -1, -1 }, { -1, -1, -1, -1 }, { -1, -1, -1, -1 }, { -1, -1, -1, -1 } };//convolve with first image
short ftprewpost4[4][4] = { { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 } };//convolve with second image
