/*
 * camera.h
 *
 *  Created on: 02.03.2017
 *      Author: Adi
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include "MyRio.h"
#include "float.h"
#include "math.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
// Vision  Includes
#include <nivision.h>
#include <NIIMAQdx.h>

#if NiFpga_Cpp
extern "C" {
#endif

bool Log_Vision_Error(int errorValue);
bool Log_Imaqdx_Error(IMAQdxError errorValue);
int capture(void);


#if NiFpga_Cpp
}
#endif

#endif /* CAMERA_H_ */
