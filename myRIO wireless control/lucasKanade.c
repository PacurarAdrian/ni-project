/*
 * lucasKanade.c
 *
 *  Created on: 23.04.2017
 *      Author: Adi
 */
#include "lucasKanade.h"

#include "float.h"
#include "math.h"
#include <stdlib.h>
#include <stdio.h>
#include "MyRio.h"
#include "functions.h"
#include <pthread.h>



/*
 * Computes the sum of products in a 3 by 3 window
 * centered at x,y of matrices f and g:
 * 			 9
 * 	@result= S[fi*gi]
 * 			i=1
 * 	@parameters
 * 	-f,g = 	vectors of more than 9 elements
 * 			vectors f,g are considered here matrices: f[x][y],
 * 	-x   = 	x describes horizontal starting position
 * 	-y	 =	y describes vertical starting position
 * 	-w 	 =	width of both matrices f and g
 *
 */
double calcSum(float *f,float *g, int x,int w, int y)
{
	int k, l;
	double sum = 0;
	for (k = -1; k < 2; k++)
	{
		for (l = -1; l < 2; l++)
		{
			sum += f[(y+k)*w + x+l] * g[(y+k)*w + x+l];
		}
	}
	return sum;
}
/*
 * Computes optical flow with Lukas-Kanade method
 * @parameters:
 * - height			=height of images corresponding to the partial derivatives dfx, dft, dfy
 * - width			=width of images corresponding to the partial derivatives dfx, dft, dfy
 * - dfx,dfy,dft	=are image intensity derivatives at (x, y, t), where x is in horizontal
 * 					y is in vertical direction an t is time (image frame number)
 * @return:
 * -Optical flow values:
 * 		- u 	= x component of image velocity or optical flow for every pixel in the image
 * 		- v		= y component of image velocity or optical flow for every pixel in the image
 * 		- umax 	= maximum value of u vector
 * 		- umin	= minimum value of u vector
 * 		- vmin 	= minimum value of v vector
 * 		- vmax 	= maximum value of v vector
 * 		- height, width = number of rows and number or columns of u and v regarded as matrices
 */
OpticalFlowValues lucasKanade(int height, int width, float* dfx, float* dfy, float*dft)
{
	OpticalFlowValues flv;
	 flv.u = (float*)malloc(height*width*sizeof(float));
	 flv.v = (float*)malloc(height*width*sizeof(float));
	 flv.umax = -FLT_MAX;
	 flv.umin = FLT_MAX;
	 flv.vmax = -FLT_MAX,
	flv.vmin = FLT_MAX;
	 flv.width = width;
	 flv.height = height;
	int w = width;
	int i, j;
	//int half = 1;
	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++)
		{
			flv.u[i*w + j] = 0;
			flv.v[i*w + j] = 0;
		}
	for (i = 1; i < height - 1; i++)
		for (j = 1; j < width - 1; j++)
		{


			float temp = calcSum(dfx, dfy, j, w, i);
			float tempx = calcSum(dfx, dfx, j, w, i);
			float tempy = calcSum(dfy, dfy, j, w, i);
			float nominator = tempx*tempy - temp*temp;
			float temp2 = calcSum(dfx, dft, j, w, i);
			float temp3 = calcSum(dfy, dft, j, w, i);
			if (nominator != 0)
			{
				flv.u[i*w + j] = (-tempy*temp2 + temp*temp3) / nominator;

				flv.v[i*w + j] = (temp2*temp - tempx*temp3) / nominator;
			}
			else {
				flv.u[i*w + j] = 0;
				flv.v[i*w + j] = 0;
			}



			if (flv.u[i*w + j] > flv.umax)
			{
				flv.umax = flv.u[i*w + j];
			}
			else if (flv.u[i*w + j] < flv.umin)
			{
				flv.umin = flv.u[i*w + j];
			}
			if (flv.v[i*w + j] > flv.vmax)
			{
				flv.vmax = flv.v[i*w + j];
			}
			else if (flv.v[i*w + j] < flv.vmin)
			{
				flv.vmin = flv.v[i*w + j];
			}
		}
	if(VERBOUSE)
	{
		printf("\numax=%lf", flv.umax);
		printf("\nvmax=%lf", flv.vmax);
		printf("\numin=%lf", flv.umin);
		printf("\nvmin=%lf\n", flv.vmin);
	}
	return flv;
}


/*
 * This function is used for calling function
 * LKopticalFlow when that function must be
 * executed in a separate thread created
 * with pthread_create
 * @params:
 * 	-dataArg - contains the input parameters of function
 * 			LKopticalFlow and also the return value of that function
 *
 */
void* LKopticalFlowThreadable(void *dataArg)
{
	struct OF_data *data;
	data=(struct OF_data *)dataArg;
	Image *src=data->src;
	Image *src2=data->src2;
	float noiseThresh=data->noiseThreshnold;
	int step=data->step;
	int maskSize=data->maskSize;
	float intensityPercentage=data->intensityPercentage;
	Image* rez=LKopticalFlow(src,src2,intensityPercentage,noiseThresh,step,maskSize);
	data->rez=rez;
	opticalFlowFinished=true;
	pthread_exit(NULL);
}



/*prepares the needed resources for computing optical flow from on images src and src2
* using Lucas-Kanade method.
*
*  Delegates the responsibility of computing
* the intensity derivatives, calls function lucasKanade, which computes optical flow
* into vectors then those values are transformed in images which are saved or returned
*
* If Macro expansion VERBOUSE !=0
* 	then the intensity derivatives images are saved on remote target as:
* 		dfx.png - derivative with respect to x
* 		dfy.png - derivative with respect to y
* 		dft.png - derivative with respect to t
* 	else those images are not saved on remote target
*
* Parameters:
* - src : first gray scale image
* - src2 : second gray scale image
* - noiseThresh : threshold used when computing derivatives of images :
* 					if the difference between the result of applying a convolution kernel to a
* 					 position in the first image and the result of applying the convolution kernel
* 					 to the same position in the second image is < threshold then in the image derivative
* 					 only the first result is taken into consideration
* 			 it is used to lower noise
* - step : used to shrink the images. if step=1 the the derivation functions will iterate pixels
*			in images one by one, if step=2 they will iterate pixels by skipping one pixel
*			(vertically and horizontally), if step=3 skip 2 pixels and so on.
*			when step =1, the rezult image and derivation images are equal in width and height
*			when step =2, they are halved in width and size;
*			...
* @return:
*  - in case of success: an color coded  image of optical flow is returned
* - in case of failure: NULL is returned and the last error is
*  	printed on screen
*
*/
Image* LKopticalFlow(Image *src, Image *src2,float intensityPercentage,float noiseThresh,int step,int maskSize)
{
	int height;// = src.rows;
	int width;// = src.cols;
	if (imaqGetImageSize(src, &width, &height)!=TRUE)
			goto errexit;
	height=height/step;
	width=width/step;
	float *pdfx = (float*)malloc(height*width*sizeof(float));
	float *pdfy = (float*)malloc(height*width*sizeof(float));
	float *pdft = (float*)malloc(height*width*sizeof(float));

	if(computeDerivatives(src,src2,pdfx,pdfy,pdft,noiseThresh,step,maskSize)!=0)
		goto errexit;

	OpticalFlowValues flv=lucasKanade(height, width, pdfx, pdfy, pdft);
	Image* rez=getFlowImage2(flv,intensityPercentage);


	free(pdfx);
	free(pdfy);
	free(pdft);
	free(flv.u);
	free(flv.v);
	return rez;
errexit:
	printf("\nerror occured in function Lucas-Kanade optical flow\n");
	if ( (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\n %s ", tempErrorText);
		imaqDispose(tempErrorText);

	}

	return NULL;
}
