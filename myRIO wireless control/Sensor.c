/*
 * Sensor.c
 *
 *  Created on: 19.07.2016
 *      Author: Adi
 */



#include "Sensor.h"

#if !defined(LoopDuration)
#define LoopDuration    60  /* How long to output the signal, in seconds */
#endif

#if !defined(LoopSteps)
#define LoopSteps       1   /* How long to step between printing, in seconds */
#endif

/**
 * Overview:
 * Demonstrates using the PING sensor. According to specification, set SIG to 1 for 5 microseconds
 * to signal start of sensor reading, enter holdoff state for 750 microseconds, wait for 18 milliseconds
 * for rising and falling edges, signaling object detection.
 * The output is the time interval in milliseconds between the rising and falling edges.
 *
 * Instructions:
 * 1. Connect SIG input/output (either 0 V or 5 V) to DIO 0 on connector B.
 * 2. Connect the GND to a ground pin on the RIO.
 * 3. Connect the 5V to a 5V pin on the RIO.
 * 4. Run this program.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */


unsigned long getRaw()
{
	unsigned long startTime;
	unsigned long currentTime;
	unsigned long finalTime;
	NiFpga_Bool dio_B0= NiFpga_True;//write initial value of 1
	MyRio_Dio B0;

	/*
     * Specify the registers that correspond to the DIO channel
     * that needs to be accessed.
     */

	B0.dir = DIOB_70DIR;
	B0.out = DIOB_70OUT;
	B0.in = DIOB_70IN;
	B0.bit = 0;
	/*
	 * Print out the desired logic level of B/DIO0.
	 */
	printf("B/DIO0 = %d\n", dio_B0);
	/*
	 * Write to channel B/DIO0 to set it to the 1 value.
	 */
	Dio_WriteBit(&B0, dio_B0);
	//hold state for 5 microseconds
	waitFor(5);



	currentTime=getMicroseconds();
	dio_B0=NiFpga_False;//write value of 0; enter holdoff state
	/*
	 * Write to channel B/DIO0 to set it to the 0 value.
	 */
	Dio_WriteBit(&B0, dio_B0);

	finalTime = currentTime + 720; // wait 720 us; the rest 30 us are taken as margin of error
	while (currentTime < finalTime)
	{
		currentTime=getMicroseconds();
	};
	currentTime=getMicroseconds();
	finalTime = currentTime + 18.5*1000+30;// 18.5 ms + 30 us for the holdoff state
	NiFpga_Bool val,prevVal;//values of 2 consecutive readings
	prevVal=Dio_ReadBit(&B0);
	//startTime=currentTime;
	//printf("receiving input:\n");
	//printf("dio B0:%d\n",dio_B0);
	while (currentTime < finalTime)//wait for an input signal or duration proportional to the distance
	{
		 /*
		 * Read from DIO channels B/DIO0.
		 * channel is on the 8-channel bank on Connector B.
		 */
		dio_B0 = Dio_ReadBit(&B0);
		//printf("dio B0:%d\n",dio_B0);
		val=dio_B0;
		currentTime=getMicroseconds();
		if(prevVal==NiFpga_False && val==NiFpga_True)//register rising edge of signal
		{
			//printf("dio B0:%d\n",dio_B0);
			startTime=currentTime;

		}else
		if(prevVal==NiFpga_True && val==NiFpga_False)//register falling edge and end loop
		{
			//printf("dio B0:%d\n",dio_B0);
			break;
		}
		prevVal=val;

	};
	if(val!=0)//write 0 value in preparation for the next reading
	{
		dio_B0=0;
		/*
		 * Write to channel B/DIO0 to set it to the 0 value.
		 */
		Dio_WriteBit(&B0, dio_B0);
	}

	return currentTime-startTime;
}
/**
 * Overview:
 * Demonstrates using the PING sensor. According to specification, set SIG to 1 for 5 microseconds
 * to signal start of sensor reading, enter holdoff state for 750 microseconds, wait for 18 milliseconds
 * for rising and falling edges, signaling object detection.
 * The output is the time interval in microseconds between the rising and falling edges.
 *
 * Instructions:
 * 1. Connect SIG input/output (either 0 V or 5 V) to DIO 0 on connector B.
 * 2. Connect the GND to a ground pin on the RIO.
 * 3. Connect the 5V to a 5V pin on the RIO.
 * 4. Run this program.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */
unsigned long getRaw2()
{
	unsigned long startTime;
	unsigned long currentTime;
	unsigned long finalTime;
	NiFpga_Bool dio_B0;//= NiFpga_True;//write initial value of 1
	MyRio_Dio B0;

	/*
     * Specify the registers that correspond to the DIO channel
     * that needs to be accessed.
     */

	B0.dir = DIOB_70DIR;
	B0.out = DIOB_70OUT;
	B0.in = DIOB_70IN;
	B0.bit = 0;



	/*
	 * Print out the desired logic level of B/DIO0.
	 */
	//printf("B/DIO0 = %d\n", dio_B0);
	/*
		 * Write to channel B/DIO0 to set it to the 0 value.
		 */
		dio_B0= NiFpga_False;//write initial value of 0
		Dio_WriteBit(&B0, dio_B0);
	/*
	 * Write to channel B/DIO0 to set it to the 1 value.
	 */
	dio_B0= NiFpga_True;//write initial value of 1
	Dio_WriteBit(&B0, dio_B0);
	//hold state for 5 microseconds
	waitFor(5);




	dio_B0=NiFpga_False;//write value of 0; enter holdoff state
	/*
	 * Write to channel B/DIO0 to set it to the 0 value.
	 */
	Dio_WriteBit(&B0, dio_B0);


	currentTime=getMicroseconds();
	finalTime = currentTime + 18844;// wait 18.844 ms for pulse
	NiFpga_Bool val,prevVal;//values of 2 consecutive readings
	prevVal=Dio_ReadBit(&B0);
	//startTime=currentTime;
	//printf("receiving input:\n");
	while (currentTime < finalTime)//wait for an input signal or duration proportional to the distance
	{
		 /*
		 * Read from DIO channels B/DIO0.
		 * channel is on the 8-channel bank on Connector B.
		 */
		dio_B0 = Dio_ReadBit(&B0);
		//printf("dio B0:%d\n",dio_B0);
		val=dio_B0;
		currentTime=getMicroseconds();
		if(prevVal==NiFpga_False && val==NiFpga_True)//register rising edge of signal
		{
			startTime=currentTime;

		}else
		if(prevVal==NiFpga_True && val==NiFpga_False)//register falling edge and end loop
		{
			break;
		}
		prevVal=val;

	}
	if(val!=0)//write 0 value in preparation for the next reading
	{
		dio_B0=0;
		/*
		 * Write to channel B/DIO0 to set it to the 0 value.
		 */
		Dio_WriteBit(&B0, dio_B0);
	}
	unsigned long echoRaw=currentTime-startTime;
	// return echo pulse if in range; zero if out-of-range
	return echoRaw;//(echoRaw<18497) ? echoRaw:0;
}
/**
 * Overview:
 * Demonstrates using the PING sensor. According to specification, set SIG to 1 for 5 microseconds
 * to signal start of sensor reading, enter holdoff state for 750 microseconds, wait for 18 milliseconds
 * for rising and falling edges, signaling object detection.
 * The output is the time interval in microseconds between the rising and falling edges.
 *
 * Instructions:
 * 1. Connect SIG input/output (either 0 V or 5 V) to DIO 0 on connector B.
 * 2. Connect the GND to a ground pin on the RIO.
 * 3. Connect the 5V to a 5V pin on the RIO.
 * 4. Run this program.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */
unsigned long getRaw3()
{
	unsigned long currentTime;
	unsigned long finalTime;
	NiFpga_Bool dio_B0;//= NiFpga_True;//write initial value of 1
	MyRio_Dio B0;

	unsigned long risingTime,fallingTime;

	/*
     * Specify the registers that correspond to the DIO channel
     * that needs to be accessed.
     */

	B0.dir = DIOB_70DIR;
	B0.out = DIOB_70OUT;
	B0.in = DIOB_70IN;
	B0.bit = 0;

	/*
	 * send trigger pulse
	 * Write to channel B/DIO0 to set it to the 0 value.
	 */
	dio_B0= NiFpga_False;//write initial value of 0
	Dio_WriteBit(&B0, dio_B0);
	waitFor(2);
	/*
	 * send trigger pulse
	 * Write to channel B/DIO0 to set it to the 1 value.
	 */
	dio_B0= NiFpga_True;//write initial value of 1
	Dio_WriteBit(&B0, dio_B0);
	//hold state for 5 microseconds
	waitFor(5);


	dio_B0=NiFpga_False;//write value of 0; enter holdoff state
	/*
	 * Write to channel B/DIO0 to set it to the 0 value.
	 */
	Dio_WriteBit(&B0, dio_B0);
	waitFor(750);//holdoff state

	//wait for rising edge of SIG signal
	currentTime=getMicroseconds();
	finalTime=currentTime+115;//wait 115 us for rising edge to occur
	while(Dio_ReadBit(&B0)!=NiFpga_True)
	{
		if(currentTime>finalTime)
		{
			perror("\nCould not detect rising edge!\n");
			return 0;
		}

		currentTime=getMicroseconds();
	}
	risingTime=currentTime;

	//wait for falling edge of SIG signal
	currentTime=getMicroseconds();
	finalTime = currentTime + 18500;// wait 18.500 ms for falling edge
	//printf("receiving falling edge:\n");
	while (Dio_ReadBit(&B0)==NiFpga_True)//wait for an input signal of duration proportional to the distance
	{
		if(currentTime>finalTime)
		{
			perror("\nCould not detect falling edge!\n");
			return 0;
		}
		currentTime=getMicroseconds();
	}
	fallingTime=currentTime;
	unsigned long echoRaw=fallingTime-risingTime;
	// return echo pulse if in range; zero if out-of-range
	return echoRaw;//(echoRaw<18497) ? echoRaw:0;
}
/*'
 * converts the width of the input signal to a distance in centimeters
 * 1us=1s/1000000
 * distance = m/s*1s/1000000*100=1m/10000
 */
float getCm(float tc)
{

	float cair=331.5+(0.6*tc);//compute air speed in m/s

	float sobject= cair*(getRaw()/10000);
	//return (getRaw2()*3/20);//raw*6,6667
	return sobject;
}

/*
 * converts the width of the input signal to a distance in meters
 * 1us=1s/1000000
 * distance = m/s*1s/1000000=1m/1000000
 */
float getM(float tc)
{

	float cair=331.5+(0.6*tc);//compute air speed in m/s

	float sobject= cair*(getRaw3()/1000000);
	return sobject;
}
