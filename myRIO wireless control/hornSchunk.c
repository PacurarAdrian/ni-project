/*
 * hornSchunk.c
 *
 *  Created on: 23.04.2017
 *      Author: Adi
 */
#include "hornSchunk.h"
#define MAX_ITERATIONS 100
#include "float.h"
#include "math.h"
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "MyRio.h"
#include "functions.h"
/*
 * Computes optical flow with Horn-Schunck method
 * @parameters:
 * - height			=height of images corresponding to the partial derivatives dfx, dft, dfy
 * - width			=width of images corresponding to the partial derivatives dfx, dft, dfy
 * - dfx,dfy,dft	=are image intensity derivatives at (x, y, t), where x is in horizontal
 * 					y is in vertical direction an t is time (image frame number)
 * - iterationThreshold = error measure, if the Eucledian distance of two successive optical flow results
 * 							is smaller than this threshold then the algorithm stops.
 * @return:
 * -Optical flow values:
 * 		- u 	= x component of image velocity or optical flow for every pixel in the image
 * 		- v		= y component of image velocity or optical flow for every pixel in the image
 * 		- umax 	= maximum value of u vector
 * 		- umin	= minimum value of u vector
 * 		- vmin 	= minimum value of v vector
 * 		- vmax 	= maximum value of v vector
 * 		- height, width = number of rows and number or columns of u and v regarded as matrices
 */
OpticalFlowValues hornSchunk(int height,int width,float* dfx,float* dfy,float* dft,float lambda,double iterationThresh)
{
	OpticalFlowValues flv;
	int i,j;
	flv.u = (float*)malloc(height*width*sizeof(float));
	flv.v = (float*)malloc(height*width*sizeof(float));
	flv.umax=FLT_MIN;
	flv.umin=FLT_MAX;
	flv.vmax=FLT_MIN;
	flv.vmin=FLT_MAX;
	flv.width = width;
	flv.height = height;


	int w = width;


	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++)
		{
			flv.u[i*w+j] = 0;
			flv.v[i*w+j] = 0;
		}
	int k = 0;
	double eucledianDist=1;
	while (k<MAX_ITERATIONS && sqrt(eucledianDist)>iterationThresh&&eucledianDist<INT32_MAX)
	{

		eucledianDist = 0;
		for (i = 1; i< height-1; i++)
			for (j = 1; j < width-1; j++)
			{
				float uprev = flv.u[i*w + j];
				float vprev = flv.v[i*w + j];
				float uav = applyLaplace((float*)flv.u, w, i, j, lap);
				float vav = applyLaplace((float*)flv.v, w, i, j, lap);

				float fx = dfx[i*w + j];
				float fy = dfy[i*w+ j];
				float ft = dft[i*w+ j];
				float P = fx*uav + fy*vav + ft;
				float D = lambda + fx*fx + fy*fy;
				flv.u[i*w + j] = uav - fx*P / D;
				flv.v[i*w + j] = vav - fy*P / D;

				eucledianDist += (flv.u[i*w + j] - uprev)*(flv.u[i*w + j] - uprev) + (flv.v[i*w + j] - vprev)*(flv.v[i*w + j] - vprev);

				if (flv.u[i*w + j]>flv.umax)
				{
					flv.umax = flv.u[i*w + j];
				}
				else if (flv.u[i*w + j] < flv.umin)
				{
					flv.umin = flv.u[i*w + j];
				}
				if (flv.v[i*w + j] > flv.vmax)
				{
					flv.vmax = flv.v[i*w + j];
				}
				else if (flv.v[i*w + j] < flv.vmin)
				{
					flv.vmin = flv.v[i*w + j];
				}

			}
		k++;
	}
	printf("\nnr iterations=%d\n", k);
	printf("minimum euclidean distance\nbetween iterations = %.10lf\n", sqrt(eucledianDist));
	return flv;
}

/*
 * This function is used for calling function
 * opticalFlow when that function must be
 * executed in a separate thread created
 * with pthread_create
 * @params:
 * 	-dataArg - contains the input parameters of function
 * 			opticalFlow and also the return value of that function
 *
 */
void* opticalFlowThreadable(void *dataArg)
{
	struct OF_data *data;
	data=(struct OF_data *)dataArg;
	Image *src=data->src;
	Image *src2=data->src2;
	float lambda=data->lambda;
	float intensityPercentage=data->intensityPercentage;
	float iterationThresh=data->iterationThresh;
	float noiseThresh=data->noiseThreshnold;
	int step=data->step;
	int maskSize=data->maskSize;
	Image* rez=opticalFlow(src,src2,lambda,intensityPercentage,iterationThresh,noiseThresh,step,maskSize);
	data->rez=rez;
	opticalFlowFinished=true;
	pthread_exit(NULL);
}


/*prepares the needed resources for computing optical flow from on images src and src2
* using Horn-Schunck method.
*
* This function sets the derivative masks, delegates the responsibility of computing
* the intensity derivatives, calls function hornSchunck, which computes optical flow
* into vectors then those values are transformed in images which are saved or returned
*
* If Macro expansion VERBOUSE !=0
* 	then the intensity derivatives images are saved on remote target as:
* 		dfx.png - derivative with respect to x
* 		dfy.png - derivative with respect to y
* 		dft.png - derivative with respect to t
* 	else those images are not saved on remote target
* parameters:
* - src : first gray scale image
* - src2 : second gray scale image
* - intensityPercentage: is the Value in HSV (hue saturation value color space), real value in [0,1]
* - noiseThresh : threshold used when computing derivatives of images :
* 					if the difference between the result of applying a convolution kernel to a
* 					 position in the first image and the result of applying the convolution kernel
* 					 to the same position in the second image is < threshold then in the image derivative
* 					 only the first result is taken into consideration
* 			 it is used to lower noise
* - step : used to shrink the images. if step=1 the the derivation functions will iterate pixels
*			in images one by one, if step=2 they will iterate pixels by skipping one pixel
*			(vertically and horizontally), if step=3 skip 2 pixels and so on.
*			when step =1, the rezult image and derivation images are equal in width and height
*			when step =2, they are halved in width and size;
*			...
* @return:
*  - in case of success: an color coded  image of optical flow is returned
* - in case of failure: NULL is returned and the last error is
*  	printed on screen
*
*/
Image* opticalFlow(Image *src, Image *src2,float lambda,float intensityPercentage,float iterationThresh,float noiseThresh,int step,int maskSize)
{
	int height;// = src.rows;
	int width;// = src.cols;
	if (Log_Vision_Error(imaqGetImageSize(src, &width, &height)))
			goto errexit;
	height=height/step;
	width=width/step;
	float *pdfx = (float*)malloc(height*width*sizeof(float));
	float *pdfy = (float*)malloc(height*width*sizeof(float));
	float *pdft = (float*)malloc(height*width*sizeof(float));

	if(computeDerivatives(src,src2,pdfx,pdfy,pdft,noiseThresh,step,maskSize)!=0)
		goto errexit;

	OpticalFlowValues flv=hornSchunk(height, width, pdfx, pdfy, pdft,lambda,iterationThresh);
	Image* rez=getFlowImage2(flv,intensityPercentage);


	free(pdfx);
	free(pdfy);
	free(pdft);
	free(flv.u);
	free(flv.v);
	return rez;
errexit:

	if ( (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\n %s ", tempErrorText);
		imaqDispose(tempErrorText);

	}
	printf("\nerror occured in function optical flow\n");
	return NULL;
}


