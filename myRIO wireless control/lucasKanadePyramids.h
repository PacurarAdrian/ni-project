/*
 * lucasKanade.h
 *
 *  Created on: 23.04.2017
 *      Author: Adi
 */

#ifndef LUCASKANADEPYRAMIDS_H_
#define LUCASKANADEPYRAMIDS_H_

#include "opticalFlow.h"
#include "lucasKanade.h"

#if NiFpga_Cpp
extern "C" {
#endif
Image* reduce(Image* img,double w[5][5]);
double bilinearInterpolate(int xo,int yo,int xu,int yu,double y,double x,float *u,int width);
double linearInterpolatex(int xo, int xu, int y, double x, float *u, int width);
double linearInterpolatey(int yo, int yu, double y,int x, float *u, int width);
Image* pyramids(Image *src, Image *src2, float noiseThresh,float intensityPercentage,int maskSize,int levels);//call with noise=0
void* LKPyramidsThreadable(void *dataArg);
#if NiFpga_Cpp
}
#endif

#endif /* LUCASKANADEPYRAMIDS_H_ */
