#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <sched.h>
#include "led.h"
#include "button.h"
#include "MyRio.h"
#include "WheelEncoder.h"
#include "Forward.h"
#include "Encoder.h"
//#include "PWM.h"
#include "SensorMotor.h"
#include "Sensor.h"
#include "hornSchunk.h"
#include "lucasKanade.h"
#include "lucasKanadePyramids.h"
//#include "functions.h"




#if !defined(LoopDuration)
#define LoopDuration    600  /* How long to output the signal, in seconds */
#endif


#if !defined(LoopSteps)
#define LoopSteps       1   /* How long to step between printing, in seconds */
#endif

volatile char command='5';


void* getCommand(void* arg) {

	do
	{

		//
		//char c;
		//while((c = getchar()) != '\n' && c != EOF){}
		//comTemp=(char)getc(stdin);
		scanf("%c\n",&command);



	}while(command!='q'&&command!='m'&& command!='p');

	pthread_exit(NULL);
}




opticalFlowMethod setParameters(struct OF_data *data)
{
	FILE *g = fopen("parameters.txt", "r");
	if(g==NULL)
	{


		g = fopen("parameters.txt", "w");
		printf("\nSetting default values for optical flow parameters.\n");
		data->lambda=0.1;
		data->intensityPercentage=3.0/4;
		data->iterationThresh=0.01;
		data->noiseThreshnold=0;
		data->step=1;
		data->maskSize=3;
		data->levels=2;

		fprintf(g,"lambda: %f\n",data->lambda);
		fprintf(g,"intensity: %f\n",data->intensityPercentage);
		fprintf(g,"threshold: %f\n",data->iterationThresh);
		fprintf(g,"noise: %f\n",data->noiseThreshnold);
		fprintf(g,"step: %d\n",data->step);
		fprintf(g,"maskSize: %d\n",data->maskSize);
		fprintf(g,"levels: %d\n",data->levels);
		fprintf(g,"method: %d\n",LucasKanade);
		fclose(g);
		return LucasKanade;


	}else{
		opticalFlowMethod method;
		int methNr;
		printf("\nReading parameters for optical flow from file\n");
		fscanf(g,"lambda: %f\n",&data->lambda);
		fscanf(g,"intensity: %f\n",&data->intensityPercentage);
		fscanf(g,"threshold: %f\n",&data->iterationThresh);
		fscanf(g,"noise: %f\n",&data->noiseThreshnold);
		fscanf(g,"step: %d\n",&data->step);
		fscanf(g,"maskSize: %d\n",&data->maskSize);
		fscanf(g,"levels: %d\n",&data->levels);
		fscanf(g,"method: %d\n",&methNr);
		method=methNr%3;
		printf("lambda: %f\n",data->lambda);
		printf("intensity: %f\n",data->intensityPercentage);
		printf("threshold: %f\n",data->iterationThresh);
		printf("noise: %f\n",data->noiseThreshnold);
		printf("step: %d\n",data->step);
		printf("maskSize: %d\n",data->maskSize);
		printf("levels: %d\n",data->levels);
		switch(method)
		{
		case LucasKanadePyramids:
			printf("method: %s\n","Lucas-Kanade with pyramids");
			break;
		case HornSchunck:
			printf("method: %s\n","Horn-Schunck");
			break;
		default :
			printf("method: %s\n","Lucas-Kanade");
			break;
		}

		fclose(g);
		return method;
	}

	//return NULL;
}

int main(int argc, char **argv)
{
    NiFpga_Status status;
//time tracking variables
    time_t ofTimeTemp;//for measuring optical flow start and end times
    time_t currentTime;
    time_t finalTime;//holds the time when the program will stop
    time_t printTime;
    time_t startTime;//holds the time that the program started
//camera variables
    struct OF_data data;
	opticalFlowMethod method;//=LucasKanadePyramids;
	opticalFlowFinished=false;
    //time_t OFTime;
   // bool OFcapture=false;
//encoder variables
    MyRio_Encoder encB0;
    MyRio_Encoder encA0;
    uint32_t stepsB;
	uint32_t stepsA;
	uint32_t stepsRefB;
	uint32_t stepsRefA;
	const char* directionA;
	const char* directionB;
//motor variables
	MyRio_Pwm pwmA2;
	MyRio_Pwm pwmA1;
	int speedLeft=0,speedRight=0;
	int gear=1;
	int direction=1;//value set 1 for forward motion else -1 for backward motion
//sensor motor variables
	int pozSensor=90;//sensor faces forward
//command thread variables
	char c;
	char prevCommand=command;
	pthread_t th1;
	pthread_t thImage;
	pthread_t thImage2;
	int code;
//sensor variables
	float temperature=22;
//button variable
	NiFpga_Bool prevButton;
//leds
	NiFpga_Bool *leds=(NiFpga_Bool*)malloc(4*sizeof(NiFpga_Bool));
	uint8_t bit;//led number
	//initialize leds
	leds[0]=NiFpga_True;//idle status
	leds[1]=NiFpga_False;//moving
	leds[2]=NiFpga_False;//optical flow
	leds[3]=NiFpga_False;//optical flow corrupted


	//read temperature
	printf("Enter temperature for ultrasonic sensor:");
	scanf("%f",&temperature);

	 /*
	 * Open the myRIO NiFpga Session.
	 * This function MUST be called before all other functions. After this call
	 * is complete the myRIO target will be ready to be used.
	 */
    status = MyRio_Open();
    if (MyRio_IsNotSuccess(status))
    {
    	printf("ERROR; return code from MyRio_Open() is %d\n", status);
        goto cleanup;
    }

    /*
     * Here is where you will need to write all the instructions for the robot(between
     * open and close opperations). When myRIO NiFpga Session is closed, all the pins
     * get the default value, so you will have to work with temporizations( see WheelEncoder
     * example).
     */

//set parameters for optical flow from file or from default
    method=setParameters(&data);

    //open thread for reading commands

   	if((code=pthread_create(&th1, NULL, getCommand, NULL))!=0)
   	{
   		printf("ERROR; return code from getCommand() is %d\n", code);
   		goto cleanup;
   	}


    status=initializeEncoders(&encB0,&encA0);
    if(MyRio_IsNotSuccess(status))
    {
    	printf("ERROR; return code from initializing encoders is %d\n", status);
    	goto cleanup;
    }


    initializePwm( &pwmA2, &pwmA1);


    status=position(pozSensor);
	if(MyRio_IsNotSuccess(status))
	{
		printf("ERROR; return code from setting sensor position is %d\n", status);
		goto cleanup;
		//return status;
	}

    /*
	 * Normally, the main function runs a long running or infinite loop.
	 * Keep the program running for 60 seconds so that the PWM output can be
	 * observed using an external instrument.
	 */

	time(&currentTime);
	startTime = currentTime;
	finalTime = currentTime + LoopDuration;
	printTime = currentTime;
	//OFTime=currentTime;

	Image *src=NULL;
	Image *src2=NULL;
	// Create the Image Buffer
	src = imaqCreateImage(IMAQ_IMAGE_U8, 0);
	src2 = imaqCreateImage(IMAQ_IMAGE_U8, 0);
	if(src==NULL || src2==NULL)
	{
		if(imaqGetLastError() != ERR_SUCCESS) {
				char *tempErrorText = imaqGetErrorText(imaqGetLastError());
				printf("\n %s ", tempErrorText);
				imaqDispose(tempErrorText);
		}
		leds[3]=NiFpga_True;
	}else
	//if(Log_Vision_Error(imaqReadFile(src, CAPTURED_IMAGE1_PATH, NULL,NULL)))
	if((code=pthread_create(&thImage, NULL, captureImage,(void*) src))!=0)
	{
		printf("ERROR; return code from creating thread captureImage() is %d\n", code);
		//goto cleanup;
		leds[3]=NiFpga_True;
	}
	showColorMap(-180, 180, -180, 180);

	//set leds
	for(bit=0;bit<4;bit++)
	{
		switchLed(bit,leds[bit]);
	}
	prevButton=buttonValue();

	while (currentTime < finalTime && prevButton==buttonValue())
	{
		time(&currentTime);


		/* Don't print every loop iteration. */
		if (currentTime > printTime)
		{


			printf("command is :%c\n",command);
			switch(command)
			{
			case '8'://go forward
				if(prevCommand!= '8')
				{
					stepsRefA=stepsA;
					stepsRefB=stepsB;
					speedRight=0;
					speedLeft=0;
					direction=1;
				}
				status=goForward2(pwmA2,pwmA1,gear,stepsA-stepsRefA,stepsB-stepsRefB,speedRight,speedLeft);
				if(MyRio_IsNotSuccess(status))
					goto cleanup;
				printf("Left: Steps %d, Direction: %s\n", stepsA-stepsRefA, directionA);
				printf("Right: Steps %d, Direction: %s\n", stepsB-stepsRefB, directionB);
				printf("%g s: going forward\n",difftime(currentTime,startTime));
			break;
			case '2'://go backwards
				if(prevCommand!= '2')
				{
					stepsRefA=stepsA;
					stepsRefB=stepsB;
					speedRight=0;
					speedLeft=0;
					direction=-1;
				}
				status=goBackward2(pwmA2,pwmA1,gear,stepsA-stepsRefA,stepsB-stepsRefB,speedRight,speedLeft);
				if(MyRio_IsNotSuccess(status))
					goto cleanup;
				printf("Left: Steps %d, Direction: %s\n", stepsA-stepsRefA, directionA);
				printf("Right: Steps %d, Direction: %s\n", stepsB-stepsRefB, directionB);
				printf("%g s: going backwards\n",difftime(currentTime,startTime));
				break;
			case '4'://steer left
				if(prevCommand!= '4')
				{
					stepsRefA=stepsA;
					stepsRefB=stepsB;
					speedRight=0;
					speedLeft=0;
				}
				status=setSpeedRight( &pwmA2,direction*gear,0);
				if(MyRio_IsNotSuccess(status))
				{
					printf("Error %d occurred while setting right motor to gear %d",status,gear);
					goto cleanup;
				}
				status=setSpeedLeft(&pwmA1,0,0);
				if(MyRio_IsNotSuccess(status))
				{
					printf("Error %d occurred while stopping left motor",status);
					return status;
				}
				printf("Left: Steps %d, Direction: %s\n", stepsA-stepsRefA, directionA);
				printf("Right: Steps %d, Direction: %s\n", stepsB-stepsRefB, directionB);
				printf("%g s: steering left\n",difftime(currentTime,startTime));
				break;
			case '6'://steer right
				if(prevCommand!= '6')
				{
					stepsRefA=stepsA;
					stepsRefB=stepsB;
					speedRight=0;
					speedLeft=0;
				}
				status=setSpeedRight( &pwmA2,0,0);
				if(MyRio_IsNotSuccess(status))
				{
					printf("Error %d occurred while stopping right motor",status);
					goto cleanup;
				}
				status=setSpeedLeft(&pwmA1,direction*gear,0);
				if(MyRio_IsNotSuccess(status))
				{
					printf("Error %d occurred while setting left motor to gear %d",status,gear);
					goto cleanup;
				}
				printf("Left: Steps %d, Direction: %s\n", stepsA-stepsRefA, directionA);
				printf("Right: Steps %d, Direction: %s\n", stepsB-stepsRefB, directionB);
				printf("%g s: steering right\n",difftime(currentTime,startTime));
				break;

			case '.'://increase degree of sensor motor
				if(pozSensor<180)
					pozSensor+=10;
				status=position(pozSensor);
				if(MyRio_IsNotSuccess(status))
				{
					printf("Error %d occurred while moving sensor to right",status);
					goto cleanup;
				}
				printf(" %g s: moving sensor to the right at %d degrees\n",difftime(currentTime,startTime),pozSensor);
				break;
			case ','://decrease degree of sensor motor
				if(pozSensor>0)
					pozSensor-=10;
				status=position(pozSensor);
				if(MyRio_IsNotSuccess(status))
				{
					printf("Error %d occurred while moving sensor to left",status);
					goto cleanup;
				}
				printf("%g s: moving sensor to the left at %d degrees\n",difftime(currentTime,startTime),pozSensor);
				break;
			case '-'://decrease gear
				if(gear>-4)
					gear--;
				printf("%g s: decreasing gear to %d\n",difftime(currentTime,startTime),gear);
				break;
			case '+'://increase gear
				if(gear<4)
					gear++;
				printf("%g s: increasing gear to %d\n",difftime(currentTime,startTime),gear);
				break;
			case 'q'://=q ==exit loop
				pthread_cancel (th1);
				//pthread_cancel (thImage);
				//if(OFcapture&&currentTime>OFTime)
				//	pthread_cancel (thImage2);
				currentTime = finalTime;
				break;
			case 's'://sensor read
				pthread_cancel( th1);
				//printf("%g : sensor raw reading: %lu\n",difftime(currentTime,startTime),getRaw2());
				printf("%g s: sensor reading: %f cm\n",difftime(currentTime,startTime),getM(temperature));
				while((c = getchar()) != 'k' && c != EOF){}
				if((code=pthread_create(&th1, NULL, getCommand, NULL))!=0)
				{
					printf("ERROR; return code from getCommand() is %d\n", code);
					goto cleanup;
				}
				break;
			case 'v':
				VERBOUSE = !VERBOUSE;
				if(VERBOUSE)
					printf("VERBOUSE=true, everything will be listed.");
				else printf("VERBOUSE=false, only basic feedback will be displayed.");
				break;
			case 'm':
				pthread_cancel( th1);
				printf("The optical flow methods are:\n 0 - Lucas-Kanade\n 1 - Horn-Schunck\n 2 - Lucas-Kanade with pyramids\n");
				printf("Enter desired method number");
				int tempMethod;
				scanf("%d",&tempMethod);
				method=tempMethod%3;
				printf("You chose method: %d\nEnter k to continue",method);

				while((c = getchar()) != 'k' && c != EOF){}
				 //open thread for reading commands
				if((code=pthread_create(&th1, NULL, getCommand, NULL))!=0)
				{
					printf("ERROR; return code from getCommand() is %d\n", code);
					goto cleanup;
				}


				break;
			case 'p':

				if(leds[2]==NiFpga_True)//optical flow processing in progress
				{
					printf("Cannot set parameters during computation");
					break;
				}
				pthread_cancel( th1);
				printf("\nEnter parameters for optical flow:\n");
				printf("Lambda: (current:%f)",data.lambda);
				scanf("%f",&data.lambda);
				printf("Intensity: (current:%f)",data.intensityPercentage);
				scanf("%f",&data.intensityPercentage);
				printf("Threshold: (current:%f)",data.iterationThresh);
				scanf("%f",&data.iterationThresh);
				printf("Noise: (current:%f)",data.noiseThreshnold);
				scanf("%f",&data.noiseThreshnold);
				printf("Step: (current:%d)",data.step);
				scanf("%d",&data.step);
				printf("Mask size: (current:%d)",data.maskSize);
				scanf("%d",&data.maskSize);
				printf("Pyramid levels: (current:%d)",data.levels);
				scanf("%d",&data.levels);

				FILE *g= fopen("parameters.txt", "w");
				fprintf(g,"lambda: %f\n",data.lambda);
				fprintf(g,"intensity: %f\n",data.intensityPercentage);
				fprintf(g,"threshold: %f\n",data.iterationThresh);
				fprintf(g,"noise: %f\n",data.noiseThreshnold);
				fprintf(g,"step: %d\n",data.step);
				fprintf(g,"maskSize: %d\n",data.maskSize);
				fprintf(g,"levels: %d\n",data.levels);
				fprintf(g,"method: %d\n",method);
				fclose(g);
				printf("Parameters saved.\nEnter k to continue.");

				while((c = getchar()) != 'k' && c != EOF){}
				if((code=pthread_create(&th1, NULL, getCommand, NULL))!=0)
				{
					printf("ERROR; return code from getCommand() is %d\n", code);
					goto cleanup;
				}

				break;
			case 'c'://camera optical flow
				if(leds[3]==NiFpga_True)//if previous optical flow error occurred
				{
					printf("\nCannot start optical flow because of previous errors!\n");
					break;
				}
				//OFcapture=true;
				if(leds[2]!=NiFpga_True)
				{
					leds[2]=NiFpga_True;//signal beginning of optical flow processing
					switchLed(2,leds[2]);
				}else {
					printf("\nDid not finish past processing please wait\n");
					break;
				}

					pthread_join( thImage, NULL);
					//open thread for snapping image
					//if(Log_Vision_Error(imaqReadFile(src2, CAPTURED_IMAGE2_PATH, NULL,NULL)))
					if((code=pthread_create(&thImage2, NULL, captureImage,(void*) src2))!=0)
					{
						printf("ERROR; return code from creating thread captureImage() is %d\n", code);
						//goto cleanup;
						leds[2]=NiFpga_False;//signal end of optical flow processing
						switchLed(2,leds[2]);
						leds[3]=NiFpga_True;
						switchLed(3,leds[3]);
						break;

					}



					pthread_join( thImage2, NULL);


					//captureImage(src2);
//					if((data.src = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
//							goto cleanup;
//					if((data.src2 = imaqCreateImage(IMAQ_IMAGE_U8, 0))==NULL)
//							goto cleanup;
//					if(!Log_Vision_Error(imaqScale(data.src,src,2,2,IMAQ_SCALE_SMALLER,IMAQ_NO_RECT)))
//					{
//						printf("\nimage1 scaled to half it's size");
//					}
//					if(!Log_Vision_Error(imaqScale(data.src2,src2,2,2,IMAQ_SCALE_SMALLER,IMAQ_NO_RECT)))
//					{
//						printf("\nimage2 scaled to half it's size");
//					}
					if(!Log_Vision_Error(imaqWriteFile(src, CAPTURED_IMAGE1_PATH, NULL)))
					{
						printf("\nimage1 saved to file on remote target\n");
					}
					if(!Log_Vision_Error(imaqWriteFile(src2, CAPTURED_IMAGE2_PATH, NULL)))
					{
						printf("\nimage2 saved to file on remote target\n");
					}
					data.src=src;
					data.src2=src2;

					time(&ofTimeTemp);
					printf("optical flow start time: %lu us, %g s ",getMicroseconds(),difftime(startTime,ofTimeTemp));
					int rc;
					switch(method)
					{
						case HornSchunck:
							rc = pthread_create(&thImage, NULL, opticalFlowThreadable, (void *)&data);
							break;
						case LucasKanadePyramids:
							rc= pthread_create(&thImage,NULL, LKPyramidsThreadable,(void*) &data);
							break;
						default:
							rc = pthread_create(&thImage, NULL, LKopticalFlowThreadable, (void *)&data);
							break;
					}


					if (rc) {
						printf("ERROR; return code from opticalFlowThreadable() is %d\n", rc);
						//goto cleanup;
						leds[2]=NiFpga_False;//signal end of optical flow processing
						switchLed(2,leds[2]);
						leds[3]=NiFpga_True;
						switchLed(3,leds[3]);
					}




				break;
			default ://stop

				if(leds[0]!=NiFpga_True)
				{
					leds[0]=NiFpga_True;//signal idle state
					switchLed(0,leds[0]);
					if(leds[1]!=NiFpga_False)
					{
						leds[1]=NiFpga_False;//signal non-movement command
						switchLed(1,leds[1]);
					}

					status=setSpeedRight( &pwmA2,0,0);
					if(MyRio_IsNotSuccess(status))
					{
						printf("Error %d occurred while stopping right motor",status);
						goto cleanup;
					}
					status=setSpeedLeft(&pwmA1,0,0);
					if(MyRio_IsNotSuccess(status))
					{
						printf("Error %d occurred while stopping left motor",status);
						goto cleanup;
					}
					printf("%g s: stopping\n",difftime(currentTime,startTime));
				}else printf("%g s: stopped\n",difftime(currentTime,startTime));
				break;
			}
			if(leds[2]&&opticalFlowFinished)
			{
				printf("OF finish time\n");
				printf("waiting for optical flow to finish...\n");
				pthread_join( thImage, NULL);
				Image* rez=data.rez;
				time(&ofTimeTemp);
				printf("optical flow finish time: %lu us, %g s ",getMicroseconds(),difftime(startTime,ofTimeTemp));
				if(!Log_Vision_Error(imaqWriteFile(rez, REZ_IMAGE_PATH, NULL)))
				{
					printf("\nResult image saved to file on remote target\n");

				}else{
					leds[3]=NiFpga_True;
					switchLed(3,leds[3]);
				}

				if(!Log_Vision_Error(imaqDuplicate(src, src2)))
				{
					printf("current image copied to previous image\n");
				}else{
					leds[3]=NiFpga_True;
					switchLed(3,leds[3]);
				}

				opticalFlowFinished=false;
				if(leds[2]!=NiFpga_False)
				{
					leds[2]=NiFpga_False;//signal optical flow completed
					switchLed(2,leds[2]);
				}

			}
			if(command=='2'||command=='5'||command=='8'||command=='4'||command=='6')
			{
				prevCommand=command;
				if(leds[1]!=NiFpga_True&&command!='5')
				{
					leds[1]=NiFpga_True;//signal movement command
					switchLed(1,leds[1]);
				}
				if(leds[0]!=NiFpga_False&&command!='5')
				{
					leds[0]=NiFpga_False;//signal exit idle state
					switchLed(0,leds[0]);
				}
			}
			else {
				command=prevCommand;
				if(leds[1]!=NiFpga_False)
				{
					leds[1]=NiFpga_False;//signal non-movement command
					switchLed(1,leds[1]);
				}
			}

			printf("Robot in gear %d\n",gear);

			//read encoders
			stepsB = Encoder_Counter(&encB0);//right encoder
			if ((Encoder_Status(&encB0) & Encoder_StDirection)
				== Encoder_Incrementing)
			{
				directionB = "incrementing";
			}
			else
			{
				directionB = "decrementing";
			}


			stepsA = -Encoder_Counter(&encA0);//left encoder (minus because the motor is inverted)
			if ((Encoder_Status(&encA0) & Encoder_StDirection)
				== Encoder_Decrementing)//decrementing because the motor is inverted
			{
				directionA = "incrementing";
			}
			else
			{
				directionA = "decrementing";
			}



			printTime += LoopSteps;


		}

	}
cleanup:
	printf("Left:Total Steps %d, Direction: %s\n", stepsA, directionA);
	printf("Right:Total Steps %d, Direction: %s\n", stepsB, directionB);



	// Dispose the images
	imaqDispose(src);
	imaqDispose(src2);
    /*
     * Close the myRIO NiFpga Session.
     * This function MUST be called after all other functions.
     */

    status = MyRio_Close();
    return status;
}
