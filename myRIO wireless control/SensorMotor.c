/*
 * SensorMotor.c
 *
 *  Created on: Sep 4, 2015
 *      Author: So
 */


#include "Forward.h"
#include <stdio.h>
#include "MyRio.h"
#include "PWM.h"
#include <time.h>

extern NiFpga_Session myrio_session;

/**
 * Overview:
 * Demonstrates moving the sensor motor.
 *
 * Instructions:
 * 1. Connect the signal wire of the sensor motor to Connector A, PWM0.
 * 2. Run this program.
 *
 * Output:
 * The program generates a PWM signals that will move the robot sensor to "p" degrees.
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */
int position(int p)
{
    NiFpga_Status status;

    	/*
    	 * This will transform the degree value in a value that the program will work with
    	 */

    	int g;
    	g=p*6.25+375;

    	MyRio_Pwm pwmA0;

        uint8_t selectReg1;


        //printf("Sensor Motor Initialization...\n");

        /*
         * Initialize the PWM struct with registers from the FPGA personality.
         */
        pwmA0.cnfg = PWMA_0CNFG;
        pwmA0.cs = PWMA_0CS;
        pwmA0.max = PWMA_0MAX;
        pwmA0.cmp = PWMA_0CMP;
        pwmA0.cntr = PWMA_0CNTR;



        /*
         * Set the waveform, enabling the PWM onboard device.
         */
        Pwm_Configure(&pwmA0, Pwm_Invert | Pwm_Mode,
                Pwm_NotInverted | Pwm_Enabled);



        /*
         * Set the clock divider. The internal PWM counter will increments at
         * f_clk / 64
         *
         * where:
         *  f_clk = the frequency of the myRIO FPGA clock (40 MHz default)
         */
        Pwm_ClockSelect(&pwmA0, Pwm_64x);



        /*
         * Set the maximum counter value.
         *
         * The counter increments at 40 MHz / 64 = 625 kHz and the counter counts
         * from 0 to 1974. The frequency of the PWM waveform is 625 kHz / 1874
         * = 333 Hz.
         */

        Pwm_CounterMaximum(&pwmA0, 1874);



        /*
         * Set the comparison counter value.
         *
         * The duty cycle is g / 1874 .
         */
        Pwm_CounterCompare(&pwmA0, g);



        /*
         * PWM outputs are on pins shared with other onboard devices. To output on
         * a physical pin, select the PWM on the appropriate SELECT register. See
         * the MUX example for simplified code to enable-disable onboard devices.
         *
         * Read the value of the SYSSELECTA register.
         */
        status = NiFpga_ReadU8(myrio_session, SYSSELECTA, &selectReg1);
        MyRio_ReturnValueIfNotSuccess(status, status,
            "Could not read from the SYSSELECTA register!")

        /*
         * Set bit2 of the SYSSELECTA register to enable PWMA_0 functionality.
         * The functionality of the bit is specified in the documentation.
         */

        selectReg1 = selectReg1 | (1 << 2);

        /*
         * Write the updated value of the SYSSELECTA register.
         */
        status = NiFpga_WriteU8(myrio_session, SYSSELECTA, selectReg1);
        MyRio_ReturnValueIfNotSuccess(status, status,
            "Could not write to the SYSSELECTA register!")



    return status;
}
