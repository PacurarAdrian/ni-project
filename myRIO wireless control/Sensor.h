/*
 * Sensor.h
 *
 *  Created on: 19.07.2016
 *      Author: Adi
 */

#ifndef SENSOR_H_
#define SENSOR_H_

#include "MyRio.h"
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "DIO.h"
#include "functions.h"
#if NiFpga_Cpp
extern "C" {
#endif



unsigned long getRaw();
unsigned long getRaw2();
float getCm(float tc);
float getM(float tc);

#if NiFpga_Cpp
}
#endif


#endif /* SENSOR_H_ */
