

#ifndef DO_h_
#define DO_h_

#include "MyRio.h"

#if NiFpga_Cpp
extern "C" {
#endif


/**
 * Registers for a particular digital O.
 */
typedef struct
{
    uint32_t out;   /**< DIO output value register */
    uint8_t bit;    /**< Bit in the register to modify */
} MyRio_Do;


/**
 * Write a voltage value to a single channel in the 8 channel bank.
 */
void Do_WriteBit(MyRio_Do* channel, NiFpga_Bool value);


#if NiFpga_Cpp
}
#endif

#endif /* DO_h_ */
