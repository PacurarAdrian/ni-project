/*
 * DI.h
 *
 *  Created on: 10.06.2018
 *      Author: Adi
 */

#ifndef DI_h_
#define DI_h_

#include "MyRio.h"

#if NiFpga_Cpp
extern "C" {
#endif


/**
 * Registers for a particular digital Input.
 */
typedef struct
{
    uint32_t in;    /**< DI input value register */
    uint8_t bit;    /**< Bit in the register to modify */
} MyRio_Di;



/**
 * Read the value of a single channel in the 8 channel bank.
 */
NiFpga_Bool Di_ReadBit(MyRio_Di* channel);

#if NiFpga_Cpp
}
#endif

#endif /* DI_h_ */
