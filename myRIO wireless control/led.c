/*
*  Created on: 10.06.2018
 *      Author: Adi
 */

#include <stdio.h>
#include "DO.h"
#include "MyRio.h"
#include "led.h"

/**
 * Overview:
 * Demonstrates using the digital output of LEDs (DO.LED). Writes the Boolean do_A0
 * on led number bit. Prints the values to the console.
 *
 *
 *
 * Note:
 * The Eclipse project defines the preprocessor symbol for the NI myRIO-1900.
 * Change the preprocessor symbol to use this example with the NI myRIO-1950.
 */
void switchLed(uint8_t bit, NiFpga_Bool do_A0)
{


    MyRio_Do A0;

    printf("Switching led %d to %d\n",bit,do_A0);

    /*
     * Specify the registers that correspond to the DO channel
     * that needs to be accessed.
     */

    A0.out = DOLED30;

    A0.bit = bit;
    /*
     * Write to channel B/DIO7 to set it to the desired value.
     */
    Do_WriteBit(&A0, do_A0);

}
