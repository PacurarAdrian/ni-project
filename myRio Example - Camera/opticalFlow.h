/*
 * opticalFlow.h
 *
 *  Created on: 09.04.2017
 *      Author: Adi
 */

#ifndef OPTICALFLOW_H_
#define OPTICALFLOW_H_

#include "MyRio.h"
#include "float.h"
#include "math.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>
// Vision  Includes
#include <nivision.h>
#include <NIIMAQdx.h>
//structure for storing parameters of function opticalFlow
struct OF_data {
    Image* src;
    Image* src2;
    float lambda;
    float intensityPercentage;
    float iterationThresh;
    float noiseThreshnold;
    Image* rez;
    int step;
};
#if NiFpga_Cpp
extern "C" {
#endif
void* opticalFlowThreadable(void *dataArg);
bool Log_Vision_Error(int errorValue);
bool Log_Imaqdx_Error(IMAQdxError errorValue);
int captureOF(void);
Image* derivate(Image *img1, Image *img2, short *mask1[], short *mask2[], int *pRez,int maskSize ,float thresh,int step);
Image* opticalFlow(Image *src, Image *src2,float lambda,float intensityPercentage,float iterationThresh,float noiseThresh,int step);

#if NiFpga_Cpp
}
#endif

#endif /* OPTICALFLOW_H_ */
