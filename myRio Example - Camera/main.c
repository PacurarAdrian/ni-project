/*
 * opticalFlow.c
 *
 *  Created on: 14.03.2017
 *      Author: Adi
 */

#include "opticalFlow.h"
#include <sys/time.h>
#include <time.h>

// Local Definitions

#define CAM_NAME    "cam3"
#define CAPTURED_IMAGE1_PATH     "./capturedImage1.png"
#define CAPTURED_IMAGE2_PATH     "./capturedImage2.png"
#define REZ_IMAGE_PATH     "./RezImage.png"

#define WINDOW_NUMBER    15    // Image Display Window Number
#define IMAQDX_ERROR_MESSAGE_LENGTH    256
#define MAX_ITERATIONS 100
#define VERBOUSE 0

short fxmask[2][2] = { { -1, 1 }, { -1, 1 } };
short fymask[2][2] = { { -1, -1 }, { 1, 1 } };
short ftmaskprev[2][2] = { { -1, -1 }, { -1, -1 } };
short ftmaskpost[2][2] = { { 1, 1 }, { 1, 1 } };
float lap[3][3] = { { 1/12.0, 1 / 6.0, 1/12.0 }, { 1 / 6.0, -1, 1 / 6.0 }, { 1/12.0, 1 / 6.0, 1/12.0 } };
short fxprew[3][3] = { { -1, 0, 1 }, { -1, 0, 1 }, { -1, 0, 1 } };
short fyprew[3][3] = { { -1, -1, -1 }, { 0, 0, 0 }, { 1, 1, 1 } };
short ftprewprev[3][3] = { { -1, -1, -1 }, { -1, -1, -1 }, { -1, -1, -1 } };
short ftprewpost[3][3] = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
short fxprew4[4][4] = { { -3, -1, 1, 3 }, { -3, -1, 1, 3 }, { -3, -1, 1, 3 }, { -3, -1, 1, 3 } };
short fyprew4[4][4] = { { -3, -3, -3, -3 }, { -1, -1, -1, -1 }, { 1, 1, 1, 1 }, { 3, 3, 3, 3 } };
short ftprewprev4[4][4] = { { -1, -1, -1, -1 }, { -1, -1, -1, -1 }, { -1, -1, -1, -1 }, { -1, -1, -1, -1 } };
short ftprewpost4[4][4] = { { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 } };

/*
 * 1. Import the project onto Eclipse IDE.
2. Connect the Camera and update the Camera name("CAM_NAME" in ImageAcquisition.c).
3. Build the Project.
	"Project Explorer->Image Acquisition->Build Configurations->Build all"
4. Copy the Executable onto the Target,
	a. 'armv7\ImageAcquisition' for ARM
    b. 'x64\ImageAcquisition' for x64 Target.
5. Output Image (Captured Image),
	a. x64 based target - Will be displayed on the Screen .
	b. armv7 based target - Write the Captured Image to "./capturedImage.png"

Note:to see the captured image, open NI MAX, expand Remote Systems and right
click on myRIO�s name and select File Transfer. Enter �admin� and click Ok.
Go to home/admin/test and click on �capturedImage2.png� to see the image.
 */
int max(int a,int b)
{
	if(a>b)
		return a;
	else return b;
}
int min(int a,int b)
{
	if(a>b)
		return b;
	else return a;
}
void sleepFor(int microseconds)
{
	struct timespec ts;
	ts.tv_sec = microseconds / 1000000;
	ts.tv_nsec = (microseconds % 1000000) * 1000;
	nanosleep(&ts, NULL);
}
unsigned long getMicroseconds()
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	unsigned long time_in_micros = 1000000 * tv.tv_sec + tv.tv_usec;
	return time_in_micros;
}
void waitFor(int microseconds)
{
	unsigned long currentTime;
	unsigned long finalTime;
	currentTime=getMicroseconds();
	finalTime = currentTime + microseconds;
	while (currentTime < finalTime)
	{
		currentTime=getMicroseconds();
	};
}
// Print the IMAQDX Error Message
bool Log_Imaqdx_Error(IMAQdxError errorValue)
{
	if (errorValue) {
		char errorText[IMAQDX_ERROR_MESSAGE_LENGTH];
		IMAQdxGetErrorString(errorValue, errorText, IMAQDX_ERROR_MESSAGE_LENGTH);
		printf("\n %s ", errorText);
		return true;
	}
	return false;
}

// Print the VISION Error Message
bool Log_Vision_Error(int errorValue)
{
	if ( (errorValue != TRUE) && (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\n %s ", tempErrorText);
		imaqDispose(tempErrorText);
		return true;
	}
	return false;
}
int applyMask(uint8_t *lpSrc,int w,int i,int j,short *mask[],int maskSize)
{
	int accm = 0;
	int half = maskSize / 2;
	int offset = (maskSize%2==0)? 1:0;
	int k,l;
	for (k = -half+offset; k < half+1; k++)
		for (l = -half+offset; l < half+1; l++)
		{
			accm += lpSrc[(i+k)*w + j+l]*mask[k+half-offset][l+half-offset];

		}
	return accm;
}
float applyLaplace(float *lpSrc, int w, int i, int j, float mask[3][3])
{
	float accm = 0;
	int k,l;
	for (k = -1; k < 2; k++)
		for (l = -1; l < 2; l++)
		{
			accm += lpSrc[(i + k)*w + j + l] * mask[k+1][l+1];

		}
	return accm;
}
Image* derivate(Image *img1, Image *img2, short *mask1[], short *mask2[], int *pRez,int maskSize ,float thresh,int step)
{
	int height;
	int width;
	int w;

	Image* flowmat=NULL;
	flowmat = imaqCreateImage(IMAQ_IMAGE_U8, 0);
	//if (Log_Imaqdx_Error(imaqGetImageSize(img1, &width, &height)))
	//		return -1;

	uint8_t *lpSrc = imaqImageToArray(img1, IMAQ_NO_RECT, &width, &height);
	uint8_t *lpSrc2 = imaqImageToArray(img2, IMAQ_NO_RECT, &width, &height);
	uint8_t *lpRez = (uint8_t*)malloc((height/step)*(width/step)*sizeof(uint8_t));
	ImageInfo info;
	int wr=width/step;
	imaqGetImageInfo( img1, &info );
	if(lpSrc==NULL||lpSrc2==NULL)
		goto errExit;
	//FILE *g = fopen("v.txt", "w");
	w=width;

	int i,j;
	int half = maskSize / 2;
	//unsigned char *pixel = info.imageStart;

	for (i = 0; i<height; i+=step)
	{
		for (j = 0; j < width; j+=step) {
			if (i < half || i >= height - half || j < half || j >= width - half)
			{
				pRez[i/step*wr + j/step] = 0;

			}else
			{
				int accm1 = applyMask(lpSrc,w,i,j, mask1,maskSize);
				int accm2=applyMask(lpSrc2,w,i,j, mask2,maskSize);
				if (abs(accm1 - accm2) < thresh)
					pRez[i/step*wr + j/step] = 2*accm1/(maskSize);
				else pRez[i/step*wr + j/step] = (accm1+accm2)/(maskSize);
			}
			lpRez[i/step*wr + j/step] = max(0, min(255, pRez[i/step*wr + j/step]));
			//pixel = (unsigned char *)info.imageStart + i * info.pixelsPerLine + j;

			//fprintf(g,"%d ",lpRez[i*w+j]);



		}
		//fprintf(g,"\n");
		//pixel += info.pixelsPerLine - info.xRes; // jump over all padding and border
	}
	//fclose(g);

	if (Log_Vision_Error(imaqArrayToImage(flowmat, lpRez, width/step,height/step)))
			goto errExit;

	imaqDispose(lpSrc);
	imaqDispose(lpSrc2);

	return flowmat;
errExit:
	if ( (imaqGetLastError() != ERR_SUCCESS)) {
		char *tempErrorText = imaqGetErrorText(imaqGetLastError());
		printf("\n %s ", tempErrorText);
		imaqDispose(tempErrorText);
		return NULL;
	}

	return NULL;
}

void getFlowVec2(int color[3],float u, float v, float umin, float umax,float vmax,float vmin,float percentage)
{
	double thresh = (umax+vmax-vmin-umin)*percentage/100.0;
	//printf("thresh %f", thresh);
	//(u[i*w + j] + umin) * 255 / (umax + umin);

	int unorm;
	if(u<-thresh)
		unorm=255 - (int)((u) * 255.0 /  umin);
	else if (u>thresh)
		unorm =255- (int)(u * 255.0 / (umax));
	else unorm = 0;
	int vnorm;
	if (v<-thresh)
		vnorm =255-(int)((v) * 255.0 / (vmin));
	else if(v>thresh)
		vnorm =255- (int)(v * 255.0 / vmax );
	else vnorm = 0;

	if (u > thresh)
	{
		if (v > thresh)
		{
			color[0]=0;
			color[1]= vnorm;
			color[2]= unorm;//up right yellow

		}
		else if (v <-thresh)
		{
			color[0]=vnorm;
			color[1]=0;
			color[2]=unorm;//down right magenta
		}else //if (vnorm == 0)
		{
			color[0]=0;
			color[1]= 0;
			color[2]= unorm;//right red
		}

	}
	else
		if (u < -thresh)
		{
			if (v > thresh)
			{
				color[0]=unorm;
				color[1]=vnorm;
				color[2]=unorm;//up left
			}
			else if (v < -thresh)
			{
				color[0]=vnorm;
				color[1]=unorm;
				color[2]=unorm;//down left
			}
			else //if (abs(v) < 0.1)
			{
				color[0]=unorm;
				color[1]=unorm;
				color[2]=unorm;//left
			}

		}

		else //if (u == 0)
		{
			if (v>thresh)
			{
				color[0]=0;
				color[1]=vnorm;
				color[2]=0;//up green
			}
			else if (v< -thresh)
			{
				color[0]=vnorm;
				color[1]=0;
				color[2]=0;//down blue
			}
			else// if (vnorm == 0)
			{
				color[0]=255;
				color[1]=255;
				color[2]=255;//nothing white (b,g,r)
			}

		}

}
char getFlowChar(float u,float v,float thresh)
{

	//printf("\narrows:\x18\x19\x1a\x1b\n");//sus jos dr stg


	if (abs(u) < thresh)
	{
		if (abs(v) < thresh)
			return'.';
		else if (v >= thresh)
			return '^';
		else //if (v < -thresh)
			return '_';
	}
	else if (u >= thresh)
	{
		if (abs(v) < thresh)
			return '>';
		else if (v >= thresh)
			return '/';
		else //if (v < -thresh)
		{
			return '\\';
		}
	}else
		if (u <= -thresh)
		{
			if (abs(v) < thresh)
				return '<';
			else if (v >= thresh)
				return '`';
			else //if (v < -thresh)
				return ',';
		}
	return '.';
}
Image* iterate(int height,int width,int* dfx,int* dfy,int* dft,float lambda,float intensityPercentage,double iterationThresh)
{
	int i,j;
	float *u = (float*)malloc(height*width*sizeof(float));
	float *v = (float*)malloc(height*width*sizeof(float));
	float umax=FLT_MIN, umin=FLT_MAX;
	float vmax=FLT_MIN, vmin=FLT_MAX;

	//Mat umat = Mat(height, width, CV_8UC1);
	//Mat vmat = Mat(height, width, CV_8UC1);
	Image* flowmat=NULL;
	//uint8_t *lpFlow=(uint8_t*)malloc(height*width*sizeof(uint8_t));// = flowmat.data;
	// Create the Image Buffer
	flowmat = imaqCreateImage(IMAQ_IMAGE_RGB, 0);
	if (Log_Vision_Error(imaqSetImageSize(flowmat,width, height)))
			goto errexit;

	int w = width;


	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++)
		{
			u[i*w+j] = 0;
			v[i*w+j] = 0;
		}
	int k = 0;
	double eucledianDist=1;
	while (k<MAX_ITERATIONS && sqrt(eucledianDist)>iterationThresh&&eucledianDist<INT32_MAX)
	{

		eucledianDist = 0;
		for (i = 1; i< height-1; i++)
			for (j = 1; j < width-1; j++)
			{
				float uprev = u[i*w + j];
				float vprev = v[i*w + j];
				float uav = applyLaplace(u, w, i, j, lap);
				float vav = applyLaplace(v, w, i, j, lap);

				int fx = dfx[i*w + j];
				int fy = dfy[i*w+ j];
				int ft = dft[i*w+ j];
				float P = fx*uav + fy*vav + ft;
				float D = lambda + fx*fx + fy*fy;
				u[i*w + j] = uav - fx*P / D;
				v[i*w + j] = vav - fy*P / D;

				eucledianDist += (u[i*w + j] - uprev)*(u[i*w + j] - uprev) + (v[i*w + j] - vprev)*(v[i*w + j] - vprev);

				if (u[i*w + j]>umax)
				{
					umax = u[i*w + j];
				}
				else if (u[i*w + j] < umin)
				{
					umin = u[i*w + j];
				}
				if (v[i*w + j] > vmax)
				{
					vmax = v[i*w + j];
				}
				else if (v[i*w + j] < vmin)
				{
					vmin = v[i*w + j];
				}

			}
		k++;
	}
	printf("\nnr iterations=%d\n", k);
	printf("minimum euclidean distance\nbetween iterations = %.10lf\n", sqrt(eucledianDist));
	if (VERBOUSE == 1)
	{
		FILE *f = fopen("u.txt", "w");
		FILE *g = fopen("v.txt", "w");
		FILE *flow = fopen("flow.txt", "w");
		if (g != NULL && f != NULL)
		{
			printf("\nsaving to files...\n");
			int color[3] = { 255, 255, 255 };
			for (i = 0; i < height; i++)
			{
				for (j = 0; j < width; j++)
				{
					fprintf(f, "%f ", u[i*width + j]);
					fprintf(g, "%f ", v[i*width + j]);
					//umat.at<uchar>(i, j) = (uchar)((u[i*w + j] - umin) * 255 / (umax - umin));
					//vmat.at<uchar>(i, j) = (uchar)((v[i*w + j] - vmin) * 255 / (vmax - vmin));
					fprintf(flow, "%c ", getFlowChar(u[i*w + j], v[i*w + j],(float) 0.1));
					//flowmat.at<Vec3b>(i, j) = getFlowVec(u[i*w + j], v[i*w + j],(float) 0.9);
					getFlowVec2(color,u[i*w + j], v[i*w + j], umin, umax, vmax, vmin, intensityPercentage);
					Point* po=(Point*)malloc(sizeof(Point));
					po->x=j;
					po->y=i;
					PixelValue p;
					RGBValue *v=(RGBValue*)malloc(sizeof(RGBValue));
					v->B=color[0];
					v->G=color[1];
					v->R=color[2];
					p.rgb=*v;
					imaqSetPixel(flowmat,*po , p);


				}
				fprintf(f, "\n");
				fprintf(g, "\n");
				fprintf(flow, "\n");
			}


			fclose(f);
			fclose(g);
			fclose(flow);
			//imshow("vmat image", vmat);
			//imshow("umat image", umat);
			//imshow("flowmat image", flowmat);
			printf("\numax=%f", umax);
			printf("\nvmax=%f", vmax);
			printf("\numin=%f", umin);
			printf("\nvmin=%f\n", vmin);
		}
		else{
			perror("could not save data to files");
		}
	}
	else {
		int color[3] = { 255, 255, 255 };


		for (i = 0; i < height; i++)
		{
			for (j = 0; j < width; j++)
			{
				getFlowVec2(color,u[i*w + j], v[i*w + j], umin, umax, vmax, vmin, intensityPercentage);
				Point* po=(Point*)malloc(sizeof(Point));
				po->x=j;
				po->y=i;
				PixelValue p;
				RGBValue *v=(RGBValue*)malloc(sizeof(RGBValue));
				v->B=color[0];
				v->G=color[1];
				v->R=color[2];
				p.rgb=*v;
				imaqSetPixel(flowmat,*po , p);
			}
		}
	}

	//if (Log_Vision_Error(imaqArrayToImage(flowmat, lpFlow, width,height)))
	//	goto errexit;

	return flowmat;
errexit:
	printf("\nerror occured in iterate function\n");
	return NULL;
}
void* opticalFlowThreadable(void *dataArg)
{
	struct OF_data *data;
	data=(struct OF_data *)dataArg;
	Image *src=data->src;
	Image *src2=data->src2;
	float lambda=data->lambda;
	float intensityPercentage=data->intensityPercentage;
	float iterationThresh=data->iterationThresh;
	float noiseThresh=data->noiseThreshnold;
	int step=data->step;
	Image* rez=opticalFlow(src,src2,lambda,intensityPercentage,iterationThresh,noiseThresh,step);
	data->rez=rez;
	pthread_exit(NULL);
}
Image* opticalFlow(Image *src, Image *src2,float lambda,float intensityPercentage,float iterationThresh,float noiseThresh,int step)
{
	int height;// = src.rows;
	int width;// = src.cols;
	if (Log_Vision_Error(imaqGetImageSize(src, &width, &height)))
			goto errexit;
	height=height/step;
	width=width/step;
//	Image* dfx;// = Mat(height, width, CV_8UC1);
//	Image* dfy, dft;
	int *pdfx = (int*)malloc(height*width*sizeof(int));
	int *pdfy = (int*)malloc(height*width*sizeof(int));
	int *pdft = (int*)malloc(height*width*sizeof(int));

	//set derivation masks:
	short* pfxmask[] = { fxprew[0], fxprew[1], fxprew[2] };
	short* pfymask[] = { fyprew[0], fyprew[1], fyprew[2] };
	short* pftmaskprev[] = { ftprewprev[0], ftprewprev[1], ftprewprev[2] };
	short* pftmaskpost[] = { ftprewpost[0], ftprewpost[1], ftprewpost[2] };
	/*char* pfxmask[] = { fxprew4[0], fxprew4[1], fxprew4[2], fxprew4[3] };
	char* pfymask[] = { fyprew4[0], fyprew4[1], fyprew4[2], fyprew4[3] };
	char* pftmaskprev[] = { ftprewprev4[0], ftprewprev4[1], ftprewprev4[2], ftprewprev4[3] };
	char* pftmaskpost[] = { ftprewpost4[0], ftprewpost4[1], ftprewpost4[2], ftprewpost4[3] };*/
//	short* pfxmask[] = { fxmask[0], fxmask[1]};
//	short* pfymask[] = { fymask[0], fymask[1]};
//	short* pftmaskprev[] = { ftmaskprev[0], ftmaskprev[1] };
//	short* pftmaskpost[] = { ftmaskpost[0], ftmaskpost[1] };

	//derivate images
	Image* d1=derivate(src, src2, pfxmask, pfxmask, pdfx, 3, noiseThresh,step);
	Image* d2=derivate(src, src2, pfymask, pfymask, pdfy, 3, noiseThresh,step);
	Image* d3=derivate(src, src2, pftmaskprev, pftmaskpost, pdft, 3, noiseThresh,step);
	if(d1==NULL||d2==NULL||d3==NULL)
	{
		goto errexit;
	}
	Image* rez=iterate(height, width, pdfx, pdfy, pdft,lambda,intensityPercentage,iterationThresh);

	if (VERBOUSE)
	{
		//Display the Captured Image (on x64 Target)
		#if defined(__x86_64__)

			RTDisplayVideoMode displayMode = IMAQ_GRAPHICS_MODE;

			// Set the Display mode to Video
			if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
				goto cleanup;
			//Display the Image
			if(Log_Vision_Error (imaqRTDisplayImage(src, WINDOW_NUMBER, 0)))
				goto cleanup;
			if(Log_Vision_Error (imaqRTDisplayImage(src2, WINDOW_NUMBER, 0)))
							goto cleanup;
			if(Log_Vision_Error (imaqRTDisplayImage(rez, WINDOW_NUMBER, 0)))
							goto cleanup;
			printf("\n Press any key to Continue... \n");
			getchar(); // Wait for user input before the image unload

			// Set the Display mode back to Text
			displayMode = IMAQ_TEXT_MODE;
			if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
					goto cleanup;
		#else
			/*int windowNumber;
			if(Log_Vision_Error(imaqGetWindowHandle(&windowNumber)))
			{
				goto cleanup;
			}else printf("\nfound free window %d\n",windowNumber);
			if(!Log_Vision_Error(imaqDisplayImage(captureImage, windowNumber, TRUE)))
			{
				printf("\nimage displayed in window\n");
			}*/
			// Write the captured image to the file

			if(!Log_Vision_Error(imaqWriteFile(d1, "./d1.png", NULL)))
			{
				printf("\nd1 saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(d2, "./d2.png", NULL)))
			{
				printf("\nd2 saved to file on remote target\n");
			}
			if(!Log_Vision_Error(imaqWriteFile(d3, "./d3.png", NULL)))
			{
				printf("\nd3 saved to file on remote target\n");
			}
			/*
			if(!Log_Vision_Error(imaqWriteFile(rez, REZ_IMAGE_PATH, NULL)))
			{
				printf("\nResult saved to file on remote target\n");
			}*/

		#endif

	}
	return rez;
errexit:
	printf("\nerror occured in function optical flow\n");
	return NULL;
}
/*
 * snap RGB image, convert to grayscale then return it
 */
void* captureImage(void *srcP)
{
	printf("entering fct\n");
	//Image *src = (Image*)srcP;

	Image* srcC=NULL;

	IMAQdxSession session = 0;

	printf("Create the Image Buffer\n");// Create the Image Buffer

	//src = imaqCreateImage(IMAQ_IMAGE_U8, 0);
	srcC = imaqCreateImage(IMAQ_IMAGE_RGB, 0);

	// Open a session to the selected camera
	if (Log_Imaqdx_Error(IMAQdxOpenCamera(CAM_NAME, IMAQdxCameraControlModeController, &session)))
		goto cleanup;

	// Acquire an image
printf("Acquire an image\n");
	if (Log_Imaqdx_Error(IMAQdxSnap(session, srcC)))
		goto cleanup;
	//if(Log_Vision_Error(imaqReadFile(srcC, "./cam1.bmp", NULL,NULL)))
	//	goto cleanup;
	//convert to grayscale
	printf("convert to grayscale\n");
	if(Log_Vision_Error(imaqExtractColorPlanes(srcC,IMAQ_HSL,NULL,NULL,(Image*)srcP)))
		goto cleanup;

//srcP=(void*)src;
cleanup:
	//Close the Camera session
	IMAQdxCloseCamera (session);

	// Dispose the image
	imaqDispose(srcC);
printf("exit\n");
	pthread_exit(NULL);
}
int main(void) {

	printf("\n ++ OpticalFlowExample \n");

	Image* src = NULL;
	Image* src2=NULL;
	//IMAQdxSession session = 0;
	pthread_t thImage;
		pthread_t thImage2;
		int code;
	// Create the Image Buffer
	src = imaqCreateImage(IMAQ_IMAGE_U8, 0);
	src2 = imaqCreateImage(IMAQ_IMAGE_U8, 0);
	//captureImage((void*)src);
	if(Log_Vision_Error(imaqReadFile(src, "./taxi1.bmp", NULL,NULL)))
					goto cleanup;
	if(Log_Vision_Error(imaqReadFile(src2, "./taxi2.jpg", NULL,NULL)))
							goto cleanup;
						printf("o citit a doua imagine\n");
	/*if((code=pthread_create(&thImage, NULL, captureImage,(void*) src))!=0)
	{
		printf("ERROR; return code from captureImage() is %d\n", code);
		goto cleanup;
	}
	pthread_join( thImage, NULL);
	//captureImage((void*)src2);
	if((code=pthread_create(&thImage2, NULL, captureImage,(void*) src2))!=0)
	{
		printf("ERROR; return code from captureImage() is %d\n", code);
		goto cleanup;
	}

	pthread_join( thImage2, NULL);
	if(!Log_Vision_Error(imaqWriteFile(src, CAPTURED_IMAGE1_PATH, NULL)))
	{
		printf("\nimage1 saved to file on remote target\n");
	}
	if(!Log_Vision_Error(imaqWriteFile(src2, CAPTURED_IMAGE2_PATH, NULL)))
	{
		printf("\nimage2 saved to file on remote target\n");
	}*/
	// Open a session to the selected camera
	//if (Log_Imaqdx_Error(IMAQdxOpenCamera(CAM_NAME, IMAQdxCameraControlModeController, &session)))
	//	goto cleanup;

	// Acquire an image
	/*
	if (Log_Imaqdx_Error(IMAQdxSnap(session, src)))
		goto cleanup;
	printf("type char..");

	waitFor(5000000);

	printf("ok.");
	if (Log_Imaqdx_Error(IMAQdxSnap(session, src2)))
			goto cleanup;

	if(Log_Vision_Error(imaqReadFile(src, "./cam1.bmp", NULL,NULL)))
			goto cleanup;
	if(Log_Vision_Error(imaqReadFile(src2, "./cam2.bmp", NULL,NULL)))
				goto cleanup;
				*/
	//Display the Captured Image (on x64 Target)
	#if defined(__x86_64__)

		RTDisplayVideoMode displayMode = IMAQ_GRAPHICS_MODE;

		// Set the Display mode to Video
		if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
			goto cleanup;
		//Display the Image
		if(Log_Vision_Error (imaqRTDisplayImage(opticalFlow(src,src2,0.1,0.5,0.01,0);, WINDOW_NUMBER, 0)))
			goto cleanup;

		printf("\n Press any key to Continue... \n");
		getchar(); // Wait for user input before the image unload

		// Set the Display mode back to Text
		displayMode = IMAQ_TEXT_MODE;
		if (Log_Vision_Error(imaqRTVideoMode(IMAQ_SET_MODE,&displayMode)))
				goto cleanup;
	#else
		//opticalFlow(src,src2,0.1,0.5,0.01,0);
		struct OF_data data;
		data.src=src;
		data.src2=src2;
		data.lambda=0.1;
		data.intensityPercentage=1.0;
		data.iterationThresh=0.01;
		data.noiseThreshnold=50;
		data.step=2;
		int rc = pthread_create(&thImage, NULL, opticalFlowThreadable, (void *)&data);
		if (rc) {
			printf("ERROR; return code from opticalFlowThreadable() is %d\n", rc);
			goto cleanup;
		}
		printf("waiting for optical flow to finish...");
		pthread_join( thImage, NULL);
		Image* rez=data.rez;
		if(!Log_Vision_Error(imaqWriteFile(rez, REZ_IMAGE_PATH, NULL)))
		{
			printf("\nimage saved to file on remote target\n");
		}
//		if(!Log_Vision_Error(imaqWriteFile(opticalFlow(src,src2,0.1,1.0,0.01,50,2), REZ_IMAGE_PATH, NULL)))
//		{
//			printf("\nimage saved to file on remote target\n");
//		}

	#endif


cleanup:
	//Close the Camera session
//	IMAQdxCloseCamera (session);

	// Dispose the image
	imaqDispose(src);
	imaqDispose(src2);


	printf("\n -- OpticalFlowExample \n");
	return 0;

}



